$(function(){
   
   $.getJSON("PageData.json", function(elementsToAdd){
	   
	   
	   jsonWebContent = elementsToAdd; 
	   
	   //Set the title appropriately
	   $("title").html(elementsToAdd.pagetitle);
	   
	   //Set the student name
	   $("#studentname").text(elementsToAdd.studentname);
	   
	   //Set the text for the banner
	   $("#bannertext").text(elementsToAdd.pagetitle);
	   
	   //Get the page links and add them
	   for(var i = 0; i < elementsToAdd.pagetitles.length; i++){
		   addPageLink(elementsToAdd.pagetitles[i], elementsToAdd.pagetitle);
	   }
	   
	   //Load in banner img
	   $("#bannerDisplay").attr("src", "imgs/" + elementsToAdd.bannerimgname);
	   
	   //Load in footer text
	   $("#footer").append(elementsToAdd.footer);
	   
	   //Reading the JSON for the webpage content
	   for(var i = 0; i < elementsToAdd.webpageContent.length; i++){
               var currentType = elementsToAdd.webpageContent[i].type;
               
               switch(currentType){
                   case "h1": addHeader(elementsToAdd.webpageContent[i].data);
                   break;
                   
                   case "p": addParagraph(elementsToAdd.webpageContent[i].data, elementsToAdd.webpageContent[i].dataFont);
                   break;
                   
                   case "ul": addUnorderedList(elementsToAdd.webpageContent[i].data);
                   break;
                   
                   case "img": addImage(elementsToAdd.webpageContent[i].data);
                   break;
                   
                   case "a": addLink(elementsToAdd.webpageContent[i].data);
                   break;
                   
                   case "video": addVideo(elementsToAdd.webpageContent[i].data);
                   break;
                   
                   case "slideshow": addSlideShow(elementsToAdd.webpageContent[i].data);
                   break;
                   
                   default: continue;
                   
               }
           }
   });
   
   function addHeader(data){
       $("#content").append("<h1>" + data + "</h1>");
       $("#content").append("</br>");
   }
   
   function addParagraph(data, dataFont){
	   var par = $("<p>"+data+"</p>");
	   par.css("font-family", "" + dataFont);
       $("#content").append(par);
       $("#content").append("</br>");
   }
   
   //Function has been updated recently by adding ".bullet" on lin 52
   function addUnorderedList(data){
       var ulTag = "<ul>";
       for(var i = 0; i < data.length; i++){
           ulTag += ("<li>" + data[i].bullet + "</li>");
       }
       ulTag += "</li>"; //Should this be </ul> ?
       $("#content").append(ulTag);
       $("#content").append("</br>");
   }
   
   //Function has been recently updated with the ability to read captions as well as floating field
   function addImage(data){
	   var caption = $("<p>" + data.caption +"</p>");
	   caption.css("float", "" + data.floating);
	   caption.css("clear", "" + data.floating);
       var img = $("<img>");
       img.attr("src", "imgs/" + data.src);
       img.width(data.width);
       img.height(data.height);
	   img.css("float", data.floating);
       $("#content").append(img);
	   $("#content").append(caption);
       $("#content").append("</br>");
   }

    function addLink(data){
        var link = $("<a>" + data.text + "</a>");
        link.attr("href", data.url);
        $("#content").append(link);
        $("#content").append("</br>");
    }
    
    function addVideo(data){
		var caption = $("<p>" + data.caption +"</p>");
        var video = $("<video></video>");
        video.attr("width", data.width);
		video.attr("height:", data.height);
        video.attr("controls", "");
        var source1 = $("<source>");
        source1.attr("src", "videos/" + data.src);
        source1.attr("type", "video/mp4");
        video.append(source1);
        $("#content").append(video);
		$("#content").append(caption);
        $("#content").append("</br>");
    }
	
	function addPageLink(data, thisPageTitle){
		var link = $("<li></li>");
		link.attr("class", "nav-bar-link");
		
		var div = $("<div></div>");
		div.attr("class", "nav-link-box");
		
		var alink = $("<a>" + data +  "</a>");
		alink.attr("href", "../" + data + "/index.html");
		alink.attr("class", "nav-link-text");
		
		div.append(alink);
		
		link.append(div);
		
		$("#page-links").append(link);
	}
    
    function addSlideShow(data){
		var totalDiv = $("<div></div>"); //The div that will contain the entire slideshow
		totalDiv.css({"overflow": "hidden"})
		var captionDiv = $("<div></div>"); //Holds the caption text
		var captionText = $("<p></p>"); //The caption text
		
		captionDiv.append(captionText);
		captionDiv.css({
			"max-width": "500px", 
			"max-height": "300px",
			"overflow": "auto"
		});
		
		
		var currentSlide = 0;
        var slideshowDiv = $("<div></div>");
		slideshowDiv.css("background-color", "black");
		slideshowDiv.width(500);
		slideshowDiv.height(500);
		var slideshowImg = $("<img></img>");
		slideshowImg.width(500);
		slideshowImg.height(500);
		console.log(data[0])
		slideshowImg.attr("src", "imgs/" + data[currentSlide].image_file_name);
		captionText.text(data[currentSlide].caption);
		slideshowDiv.append(slideshowImg);
        
		var prev = $("<button>Previous</button>");
		var play = $("<button>Play</button>");
		var next = $("<button>Next</button>");
		var pause = $("<button>Pause</button>");
		
		var willStop = 0; //Used for playing and pausing the slideshow
		
		next.click(function(){
			currentSlide = currentSlide + 1;
			if(currentSlide >= data.length){
				currentSlide = 0;
			}
			slideshowImg.attr("src", "imgs/" + data[currentSlide].image_file_name);
			captionText.text(data[currentSlide].caption);
		});
		
		prev.click(function(){
			currentSlide = currentSlide - 1;
			if(currentSlide < 0){
				currentSlide = data.length - 1;
			}
			slideshowImg.attr("src", "imgs/" + data[currentSlide].image_file_name);
			captionText.text(data[currentSlide].caption);
		});
		
		play.click(function(){
				play.replaceWith(pause);
				willStop = setInterval(function(){
					currentSlide = currentSlide + 1;
				if(currentSlide >= data.length){
					currentSlide = 0;
				}
				slideshowImg.attr("src", "imgs/" + data[currentSlide].image_file_name);
				captionText.text(data[currentSlide].caption);
			}, 3000);
		});
		
		pause.click(function(){
			pause.replaceWith(play);
			clearInterval(willStop);
		})
		
        totalDiv.append(slideshowDiv);
		totalDiv.append(captionDiv);
		totalDiv.append(prev);
		totalDiv.append(play);
		totalDiv.append(next);
		$("#content").append(totalDiv);
        $("#content").append("</br>");
        
        
    }
});