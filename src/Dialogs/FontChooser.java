/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dialogs;

import static eportgen.StartupConstants.UI_CSS;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *CLASS HELPS WITH SELECTION OF FONT 
 * @author Manan Singh
 */
public class FontChooser {
    
    private String[] fontOptions = {"Oswald", "Ubuntu", "Francois One", "Montserrat", "Roboto"};
    private ToggleGroup fontGroup = new ToggleGroup();
    //THIS IS THE TEXT THAT WILL BE UPDATED WHENERVER A RADIO BUTTON IS PRESSED
    private Text renderedText;
    
    //BUTTONS IN THE TOGGLE GROUP
    RadioButton oswald = new RadioButton(fontOptions[0]);
    RadioButton ubuntu = new RadioButton(fontOptions[1]);
    RadioButton francois_One = new RadioButton(fontOptions[2]);
    RadioButton montserrat = new RadioButton(fontOptions[3]);
    RadioButton roboto = new RadioButton(fontOptions[4]);
    
    
    public FontChooser(Text renderedText){
        this.renderedText = renderedText;
        
        oswald.setToggleGroup(fontGroup);
        ubuntu.setToggleGroup(fontGroup);
        francois_One.setToggleGroup(fontGroup);
        montserrat.setToggleGroup(fontGroup);
        roboto.setToggleGroup(fontGroup);
        
        //BY DEFAULT SET OSWALD TO BE THE SELECTED FONT
        oswald.setSelected(true);
        setChosenFont();
    }
    
    public void createDialog(){
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        VBox root = new VBox();
        root.setPadding(new Insets(10, 10, 10, 10));
        root.setSpacing(10);
        Scene scene = new Scene(root, 500, 300);
        
        scene.getStylesheets().add(UI_CSS);
        root.getStyleClass().add("componentEditView-style");
        
        Text promptText = new Text("Choose a font: ");
        
        //ADD THE TOGGLE BUTTONS
        root.getChildren().addAll(promptText, oswald, ubuntu, francois_One, montserrat, roboto);
        oswald.setOnAction((ActionEvent e) -> {
            setChosenFont();
        });
        ubuntu.setOnAction((ActionEvent e) -> {
            setChosenFont();
        });
        francois_One.setOnAction((ActionEvent e) -> {
            setChosenFont();
        });
        montserrat.setOnAction((ActionEvent e) -> {
            setChosenFont();
        });
        roboto.setOnAction((ActionEvent e) -> {
            setChosenFont();
        });
        
        stage.setScene(scene);
        stage.showAndWait();
    }
    
    //SETS THE FONT BASED ON WHICH RADIO BUTTON IS CHOSEN
    private void setChosenFont(){
        String chosen = ( (RadioButton) fontGroup.getSelectedToggle()).getText();
        renderedText.setText(chosen);
    }
    
    public String getCurrentFont(){
        String chosen = ( (RadioButton) fontGroup.getSelectedToggle()).getText();
        return chosen;
    }
    
    public void selectFont(String fontName){
        switch (fontName){
            case "Oswald":
                oswald.setSelected(true);
                renderedText.setText(fontOptions[0]);
                break;
            case "Ubuntu":
                ubuntu.setSelected(true); 
                renderedText.setText(fontOptions[1]);
                break;
            case "Francois One":
                francois_One.setSelected(true);
                renderedText.setText(fontOptions[2]);
                break;
            case "Montserrat":
                montserrat.setSelected(true);
                renderedText.setText(fontOptions[3]);
                break;
            case "Roboto":
                roboto.setSelected(true);
                renderedText.setText(fontOptions[4]);
                break;
            default:
                
        }
    }
}
