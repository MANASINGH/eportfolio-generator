/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dialogs;

import static eportgen.StartupConstants.UI_CSS;
import eportgen.model.EPortModel;
import eportgen.model.Page;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *CLASS HELPS WITH SELECTION OF FONT 
 * @author Manan Singh
 */
public class PageTitleChooser {
    
    private EPortModel model;
    private String currentTitle;
    private Page page;
    private TextField titleField = new TextField();
    
    
    public PageTitleChooser(EPortModel model, Page page){
        this.page = page;
        this.currentTitle = page.getPageTitle();
        this.model = model;
    }
    
    public void createDialog(){
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        VBox root = new VBox();
        root.setPadding(new Insets(10, 10, 10, 10));
        root.setSpacing(10);
        root.setAlignment(Pos.CENTER);
        Scene scene = new Scene(root, 500, 300);
        
        scene.getStylesheets().add(UI_CSS);
        root.getStyleClass().add("componentEditView-style");
        
        Button ok = new Button("OK");
        
        
        Text promptText = new Text("Write a title for the current page below: ");
        
        ok.setOnAction((ActionEvent e) -> {
            String titleToSet = titleField.getText();
            
            //IF THE TITLE DOESN'T ALREADY EXIST FOR A PAGE 
            if(!model.isAlreadyAPage(titleToSet)){
                AlertDialog.alert("A page with that title already exists, please choose another one!");
            }else{
                page.setPageTitle(titleToSet);
                stage.close();
            }
            
        });
        
        root.getChildren().addAll(promptText, titleField, ok);
        
        stage.setScene(scene);
        stage.showAndWait();
    }

    /**
     * @return the currentTitle
     */
    public String getCurrentTitle() {
        return currentTitle;
    }

    /**
     * @param currentTitle the currentTitle to set
     */
    public void setCurrentTitle(String currentTitle) {
        this.currentTitle = currentTitle;
    }
    
    
}
