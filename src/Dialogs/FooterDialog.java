/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dialogs;

import static eportgen.StartupConstants.UI_CSS;
import eportgen.control.PagesControl;
import eportgen.model.EPortModel;
import eportgen.model.Page;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *CLASS HELPS WITH SELECTION OF FONT 
 * @author Manan Singh
 */
public class FooterDialog {
    
    private Page page;
    private TextField footerField = new TextField();
    
    
    public FooterDialog(Page page){
        this.page = page;
        if(!page.getFooter().equals("")){
            footerField.setText(page.getFooter());
        }
    }
    
    public void createDialog(){
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        VBox root = new VBox();
        root.setPadding(new Insets(10, 10, 10, 10));
        root.setSpacing(10);
        root.setAlignment(Pos.CENTER);
        Scene scene = new Scene(root, 500, 300);
        
        scene.getStylesheets().add(UI_CSS);
        root.getStyleClass().add("componentEditView-style");
        
        Button ok = new Button("OK");
        
        
        Text promptText = new Text("Change the footer of this page to: ");
        
        ok.setOnAction((ActionEvent e) -> {
            String nameToSet = footerField.getText();
            
            if(!page.getFooter().equals(nameToSet)){
                page.setFooter(nameToSet);
                PagesControl.signalChange();
                stage.close();
            }else{
                AlertDialog.alert("That is already the footer!");
            }
            
            
        });
        
        root.getChildren().addAll(promptText, footerField, ok);
        
        stage.setScene(scene);
        stage.showAndWait();
    }
}
