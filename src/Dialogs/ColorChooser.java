/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dialogs;

import static eportgen.StartupConstants.UI_CSS;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *CLASS HELPS WITH SELECTION OF FONT 
 * @author Manan Singh
 */
public class ColorChooser {
    
    private String[] colorOptions = {"Color Scheme 1", "Color Scheme 2", "Color Scheme 3", "Color Scheme 4", "Color Scheme 5"};
    private ToggleGroup colorGroup = new ToggleGroup();
    //THIS IS THE TEXT THAT WILL BE UPDATED WHENERVER A RADIO BUTTON IS PRESSED
    private Text renderedText;
    
    //BUTTONS IN THE TOGGLE GROUP
    RadioButton color1 = new RadioButton(colorOptions[0]);
    RadioButton color2 = new RadioButton(colorOptions[1]);
    RadioButton color3 = new RadioButton(colorOptions[2]);
    RadioButton color4 = new RadioButton(colorOptions[3]);
    RadioButton color5 = new RadioButton(colorOptions[4]);
    
    
    public ColorChooser(Text renderedText){
        this.renderedText = renderedText;
        
        color1.setToggleGroup(colorGroup);
        color2.setToggleGroup(colorGroup);
        color3.setToggleGroup(colorGroup);
        color4.setToggleGroup(colorGroup);
        color5.setToggleGroup(colorGroup);
        
        //BY DEFAULT SET OSWALD TO BE THE SELECTED FONT
        color1.setSelected(true);
        setChosenColor();
    }
    
    public void createDialog(){
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        VBox root = new VBox();
        root.setPadding(new Insets(10, 10, 10, 10));
        root.setSpacing(10);
        Scene scene = new Scene(root, 500, 300);
        
        scene.getStylesheets().add(UI_CSS);
        root.getStyleClass().add("componentEditView-style");
        
        Text promptText = new Text("Choose a layout: ");
        
        //ADD THE TOGGLE BUTTONS
        root.getChildren().addAll(promptText, color1, color2, color3, color4, color5);
        color1.setOnAction((ActionEvent e) -> {
            setChosenColor();
        });
        color2.setOnAction((ActionEvent e) -> {
            setChosenColor();
        });
        color3.setOnAction((ActionEvent e) -> {
            setChosenColor();
        });
        color4.setOnAction((ActionEvent e) -> {
            setChosenColor();
        });
        color5.setOnAction((ActionEvent e) -> {
            setChosenColor();
        });
        
        stage.setScene(scene);
        stage.showAndWait();
    }
    
    //SETS THE FONT BASED ON WHICH RADIO BUTTON IS CHOSEN
    private void setChosenColor(){
        String chosen = ( (RadioButton) colorGroup.getSelectedToggle()).getText();
        renderedText.setText(chosen);
    }
    
    public String getCurrentColor(){
        String chosen = ( (RadioButton) colorGroup.getSelectedToggle()).getText();
        return chosen;
    }
    
    public void selectColor(String fontName){
        switch (fontName){
            case "Color Scheme 1":
                color1.setSelected(true);
                renderedText.setText(colorOptions[0]);
                break;
            case "Color Scheme 2":
                color2.setSelected(true); 
                renderedText.setText(colorOptions[1]);
                break;
            case "Color Scheme 3":
                color3.setSelected(true);
                renderedText.setText(colorOptions[2]);
                break;
            case "Color Scheme 4":
                color4.setSelected(true);
                renderedText.setText(colorOptions[3]);
                break;
            case "Color Scheme 5":
                color5.setSelected(true);
                renderedText.setText(colorOptions[4]);
                break;
            default:
                
        }
    }
}
