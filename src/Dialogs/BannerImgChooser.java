/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dialogs;

import static eportgen.StartupConstants.DEFAULT_PICTURE;
import static eportgen.StartupConstants.IMAGES_PATH;
import static eportgen.StartupConstants.UI_CSS;
import eportgen.model.Page;
import java.io.File;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *CLASS HELPS WITH SELECTION OF FONT 
 * @author Manan Singh
 */
public class BannerImgChooser {
    
    private Page page;
    private Text renderedText;
    private File imageFile = new File(DEFAULT_PICTURE);
    
    
    public BannerImgChooser(Page page, Text renderedText){
        this.page = page;
        this.renderedText = renderedText;
    }
    
    public void createDialog(){
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        VBox root = new VBox();
        root.setPadding(new Insets(10, 10, 10, 10));
        root.setSpacing(10);
        root.setAlignment(Pos.CENTER);
        Scene scene = new Scene(root, 500, 300);
        
        scene.getStylesheets().add(UI_CSS);
        root.getStyleClass().add("componentEditView-style");
        
        Button selectImage = new Button("Select Image");
        
        
        Text promptText = new Text("Choose a banner imgage by clicking the button below: ");
        
        selectImage.setOnAction((ActionEvent e) -> {
           chooseImage();
        });
        
        root.getChildren().addAll(promptText, selectImage);
        
        stage.setScene(scene);
        stage.showAndWait();
    }
    
    private void chooseImage(){
        FileChooser imageFileChooser = new FileChooser();
	
	// SET THE STARTING DIRECTORY
	imageFileChooser.setInitialDirectory(new File(IMAGES_PATH));
	
	// LET'S ONLY SEE IMAGE FILES
	FileChooser.ExtensionFilter jpgFilter = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.JPG");
	FileChooser.ExtensionFilter pngFilter = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.PNG");
	FileChooser.ExtensionFilter gifFilter = new FileChooser.ExtensionFilter("GIF files (*.gif)", "*.GIF");
	imageFileChooser.getExtensionFilters().addAll(jpgFilter, pngFilter, gifFilter);
	
	// LET'S OPEN THE FILE CHOOSER
	File file = imageFileChooser.showOpenDialog(null);
	if (file != null) {
	    String path = file.getPath().substring(0, file.getPath().indexOf(file.getName()));
	    String fileName = file.getName();
	    imageFile = file;
            updateImageFile();
	}	    
	else {
            AlertDialog.alert("You didn't select a valid image file!");
            
	}
    }
    
    public void setImageFile(File file){
        imageFile = file;
    }
    
    public void updateImageFile(){
        if(imageFile != null){
            page.setBannerImgPath(imageFile.getParent() + "\\"); 
            page.setBannerImgName(imageFile.getName());
            renderedText.setText(imageFile.getName()); 
        }
    }
    
    public File getImageFile(){
        return imageFile;
    }
    
}
