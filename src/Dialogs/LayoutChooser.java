/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dialogs;

import static eportgen.StartupConstants.UI_CSS;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *CLASS HELPS WITH SELECTION OF FONT 
 * @author Manan Singh
 */
public class LayoutChooser {
    
    private String[] layoutOptions = {"Layout 1", "Layout 2", "Layout 3", "Layout 4", "Layout 5"};
    private ToggleGroup layoutGroup = new ToggleGroup();
    //THIS IS THE TEXT THAT WILL BE UPDATED WHENERVER A RADIO BUTTON IS PRESSED
    private Text renderedText;
    
    //BUTTONS IN THE TOGGLE GROUP
    RadioButton layout1 = new RadioButton(layoutOptions[0]);
    RadioButton layout2 = new RadioButton(layoutOptions[1]);
    RadioButton layout3 = new RadioButton(layoutOptions[2]);
    RadioButton layout4 = new RadioButton(layoutOptions[3]);
    RadioButton layout5 = new RadioButton(layoutOptions[4]);
    
    
    public LayoutChooser(Text renderedText){
        this.renderedText = renderedText;
        
        layout1.setToggleGroup(layoutGroup);
        layout2.setToggleGroup(layoutGroup);
        layout3.setToggleGroup(layoutGroup);
        layout4.setToggleGroup(layoutGroup);
        layout5.setToggleGroup(layoutGroup);
        
        //BY DEFAULT SET OSWALD TO BE THE SELECTED FONT
        layout1.setSelected(true);
        setChosenLayout();
    }
    
    public void createDialog(){
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        VBox root = new VBox();
        root.setPadding(new Insets(10, 10, 10, 10));
        root.setSpacing(10);
        Scene scene = new Scene(root, 500, 300);
        
        scene.getStylesheets().add(UI_CSS);
        root.getStyleClass().add("componentEditView-style");
        
        Text promptText = new Text("Choose a layout: ");
        
        //ADD THE TOGGLE BUTTONS
        root.getChildren().addAll(promptText, layout1, layout2, layout3, layout4, layout5);
        layout1.setOnAction((ActionEvent e) -> {
            setChosenLayout();
        });
        layout2.setOnAction((ActionEvent e) -> {
            setChosenLayout();
        });
        layout3.setOnAction((ActionEvent e) -> {
            setChosenLayout();
        });
        layout4.setOnAction((ActionEvent e) -> {
            setChosenLayout();
        });
        layout5.setOnAction((ActionEvent e) -> {
            setChosenLayout();
        });
        
        stage.setScene(scene);
        stage.showAndWait();
    }
    
    //SETS THE FONT BASED ON WHICH RADIO BUTTON IS CHOSEN
    private void setChosenLayout(){
        String chosen = ( (RadioButton) layoutGroup.getSelectedToggle()).getText();
        renderedText.setText(chosen);
    }
    
    public String getCurrentLayout(){
        String chosen = ( (RadioButton) layoutGroup.getSelectedToggle()).getText();
        return chosen;
    }
    
    public void selectFont(String fontName){
        switch (fontName){
            case "Layout 1":
                layout1.setSelected(true);
                renderedText.setText(layoutOptions[0]);
                break;
            case "Layout 2":
                layout2.setSelected(true); 
                renderedText.setText(layoutOptions[1]);
                break;
            case "Layout 3":
                layout3.setSelected(true);
                renderedText.setText(layoutOptions[2]);
                break;
            case "Layout 4":
                layout4.setSelected(true);
                renderedText.setText(layoutOptions[3]);
                break;
            case "Layout 5":
                layout5.setSelected(true);
                renderedText.setText(layoutOptions[4]);
                break;
            default:
                
        }
    }
}
