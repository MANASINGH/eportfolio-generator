package Dialogs;

import eportgen.StartupConstants;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;


/**
 *
 * @author Manan Singh
 */
public class SaveFilePrompt {
    
    private static boolean shouldBeSaved = false;
    private static boolean cancelled = true;
    
    public static boolean execute(){
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        VBox root = new VBox();
        root.setSpacing(10);
        root.setAlignment(Pos.CENTER);
        root.setPadding(new Insets(10, 10, 10, 10));
        Text prompt = new Text("Save work for this portfolio?");
        HBox answerArea = new HBox();
        answerArea.setSpacing(15);
        answerArea.setAlignment(Pos.CENTER);
        Button yes = new Button("Yes");
        yes.setOnAction(new EventHandler<ActionEvent>(){
            public void handle(ActionEvent e){
                shouldBeSaved = true;
                cancelled = false;
                stage.close(); 
            }
        });
        Button no = new Button("No");
        no.setOnAction(new EventHandler<ActionEvent>(){
            public void handle(ActionEvent e){
                shouldBeSaved = false;
                cancelled = false;
                stage.close();
            }
        });
        root.getChildren().addAll(prompt, answerArea);
        answerArea.getChildren().addAll(yes, no);
        Scene scene = new Scene(root, 500, 100);
        scene.getStylesheets().add(StartupConstants.UI_CSS);
        root.getStyleClass().add("componentEditView-style");
        stage.setScene(scene);
        stage.showAndWait();
        return shouldBeSaved;
    }
    
    public boolean wasCancelled(){
        return cancelled;
    }
    
    public void refresh(){
        shouldBeSaved = false;
        cancelled = true;
    }
    
}
