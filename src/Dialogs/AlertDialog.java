/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dialogs;

import static eportgen.StartupConstants.UI_CSS;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author Manan Singh
 */
public class AlertDialog {
    
    public static void alert(String s){
        Stage stage = new Stage();
        stage.setTitle("Alert");
        VBox root = new VBox();
        root.setAlignment(Pos.CENTER);
        root.setPadding(new Insets(10, 10, 10, 10));
        Text errorMessage = new Text(s);
        errorMessage.setWrappingWidth(250);
        root.getChildren().add(errorMessage);
        Scene scene = new Scene(root, 400, 100);
        scene.getStylesheets().add(UI_CSS);
        root.getStyleClass().add("componentEditView-style");
        stage.setScene(scene);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.showAndWait();
    }
}
