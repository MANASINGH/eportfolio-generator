/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dialogs;

import eportgen.StartupConstants;
import eportgen.model.EPortModel;
import eportgen.model.Page;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author Manan Singh
 */
public class SaveAsFilePrompt {
    private static boolean shouldBeSaved = false;
    private static boolean cancelled = true;
    private static EPortModel model;
    
    public SaveAsFilePrompt(EPortModel model){
        this.model = model;
    }
    
    public static boolean execute(){
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        VBox root = new VBox();
        root.setSpacing(10);
        root.setAlignment(Pos.CENTER);
        Text prompt = new Text("What would you like to name this eportfolio?");
        TextField nameArea = new TextField();
        Button ok = new Button("OK");
        ok.setOnAction(new EventHandler<ActionEvent>(){
            public void handle(ActionEvent e){
                boolean verified = verifyPageName(nameArea.getText());
                if(verified){
                    model.setTitle(nameArea.getText());
                    
                    shouldBeSaved = true;
                    cancelled = false;
                    stage.close();
                }else{
                    AlertDialog.alert("This is not a valid name");
                }
            }
        });
        root.getChildren().addAll(prompt, nameArea, ok);
        Scene scene = new Scene(root, 500, 100);
        scene.getStylesheets().add(StartupConstants.UI_CSS);
        root.getStyleClass().add("componentEditView-style");
        stage.setScene(scene);
        stage.showAndWait();
        return shouldBeSaved;
    }
    
    public boolean wasCancelled(){
        return cancelled;
    }
    
    public void refresh(){
        shouldBeSaved = false;
        cancelled = true;
        model = null;
    }
    
    private static boolean verifyPageName(String s){
        if(s.equals("")){
            return false;
        }
        for(Page p: model.getPages()){
            if(p.getPageTitle().equals(s)){
                return false;
            }
        }
        return true;
    }
    
}
