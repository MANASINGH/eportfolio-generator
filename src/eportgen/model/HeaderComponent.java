/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportgen.model;

import eportgen.control.HeaderEditControl;

/**
 *
 * @author Manan Singh
 */
public class HeaderComponent extends Component {
    
    private String text = "";
    
    public HeaderComponent(){
        
    }
    
    public HeaderComponent(String text){
        this.text = text;
    }
    
    //USED IF THE USER WANTS THIS COMPONENT TO BE EDITED. A HEADEREDITCONTROL DIALOG WILL BE GENERATED
    public void edit(){
        HeaderEditControl hec = new HeaderEditControl(this);
        hec.activateEditMode();
        hec.generateDialog();
    }
    
    public String toString(){
        return "Header";
    }

    /**
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     * @param text the text to set
     */
    public void setText(String text) {
        this.text = text;
    }
}
