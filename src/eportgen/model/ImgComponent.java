/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportgen.model;

import static eportgen.StartupConstants.DEFAULT_PICTURE;
import eportgen.control.ImgEditControl;
import java.io.File;

/**
 *
 * @author Manan Singh
 */
public class ImgComponent extends Component {
    
    private File imageFile;
    private String imagePath;
    private int width;
    private int length;
    private String caption;
    //INTEGERS 1, 2, AND 3 ARE USED FOR REPRESENTING FLOATING LEFT, NEITHER, AND RIGHT
    private int floating = 2;
    
    public ImgComponent(){
        
    }
    
    public ImgComponent(File imageFile, int width, int length, String caption, int floating){
        setImageFile(imageFile);
        this.width = width;
        this.length = length;
        this.caption = caption;
        this.floating = floating;
    }
    
    public ImgComponent(String imageFile, int width, int length, String caption, int floating){
        File file = new File(imageFile);
        if(file.exists()){
            setImageFile(file);
        }
        this.width = width;
        this.length = length;
        this.caption = caption;
        this.floating = floating;
    }
    
    public ImgComponent(int width, int length, String caption, int floating){
        this(DEFAULT_PICTURE, width, length, caption, floating);
    }
    
    //USED IF THE USER WANTS THIS COMPONENT TO BE EDITED. A IMAGEEDITCONTROL DIALOG WILL BE GENERATED
    public void edit(){
        ImgEditControl iec = new ImgEditControl(this);
        iec.activateEditMode();
        iec.generateDialog();
    }
    
    public String getImagePath(){
        return imagePath;
    }
    
    public String toString(){
        return "Image";
    }

    /**
     * @return the imageFile
     */
    public File getImageFile() {
        return imageFile;
    }

    /**
     * @param imageFile the imageFile to set
     */
    public void setImageFile(File imageFile) {
        this.imageFile = imageFile;
        String pathName = imageFile.getParent() + "\\";
        this.imagePath = pathName;
    }

    /**
     * @return the width
     */
    public int getWidth() {
        return width;
    }

    /**
     * @param width the width to set
     */
    public void setWidth(int width) {
        this.width = width;
    }

    /**
     * @return the height
     */
    public int getLength() {
        return length;
    }

    /**
     * @param length the height to set
     */
    public void setLength(int length) {
        this.length = length;
    }

    /**
     * @return the caption
     */
    public String getCaption() {
        return caption;
    }

    /**
     * @param caption the caption to set
     */
    public void setCaption(String caption) {
        this.caption = caption;
    }

    /**
     * @return the floating
     */
    public int getFloating() {
        return floating;
    }

    /**
     * @param floating the floating to set
     */
    public void setFloating(int floating) {
        this.floating = floating;
    }
    
    public String getFloatingSide(){
        if(this.floating == 1){
            return "left";
        }else if(this.floating == 2){
            return "";
        }else if(this.floating == 3){
            return "right";
        }else{
            return "IDK, something went wrong...";
        }
    }
}
