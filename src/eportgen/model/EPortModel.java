/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportgen.model;

import eportgen.view.EPortView;
import java.util.ArrayList;

/**
 *
 * @author Manan Singh
 */
public class EPortModel {
    
    //DEFAULT TITLE
    private static final String DEFAULT_TITLE = "New Portfolio";
    private static final String DEFAULT_STUDENT_NAME = "New Student";
    
    //THE VIEW THAT WORK OFF THE DATA RETRIEVED FROM THE MODEL
    private EPortView view;
    
    //THE TITLE OF THIS EPORTFOLIO
    private String title = DEFAULT_TITLE;
    //THE STUDENT NAME FOR THIS PORTFOLIO
    private String studentName = DEFAULT_STUDENT_NAME;
    
    //THE PAGES THAT THE MODEL KEEPS TRACK OF 
    private ArrayList<Page> pages = new ArrayList<Page>();
    
    //AN INT THAT MAKES IT SO THAT EVERY NEW PAGE WILL BE CONCATENATED WITH THE INT SO THAT NO TWO PAGES HAVE THE SAME TITLE
    private int newPageCount = 0;
    
    public EPortModel(EPortView view){
        this.view = view;
    }
    
    public void addNewPage(){
        Page p = new Page();
        newPageCount++;
        p.setPageTitle("New Page " + Integer.toString(newPageCount));
        pages.add(p);
        //ADD A NEW TAB TO THE MODEL WHICH WILL CONTAIN A PAGE EDIT VIEW BASED OFF THE PAGE YOU ARE SENDING THROUGH THE BELOW METHOD
        view.addNewTab(p);
        
        view.enableSaveButton();
        view.enableSaveAsButton();
        view.enableExportButton();
        view.enableViewSiteButton();
        view.enableRemovePage();
        view.enableEditPageTitle();
        view.enableEditStudentName();
    }
    
    public void addPage(Page p){
        newPageCount++;
        pages.add(p);
        //ADD A NEW TAB TO THE VIEW WHICH WILL CONTAIN A PAGE EDIT VIEW BASED OFF THE PAGE YOU ARE SENDING THROUGH THE BELOW METHOD
        view.addNewTab(p);
        
        view.enableSaveButton();
        view.enableSaveAsButton();
        view.enableExportButton();
        view.enableViewSiteButton();
        view.enableRemovePage();
        view.enableEditPageTitle();
        view.enableAddPage();
        view.enableEditStudentName();
    }
    
    public ArrayList<Page> getPages(){
        return pages;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }
    
    //REMOVES A PAGE FROM THE MODEL DATA
    public void removePage(Page p){
        //FIRST REMOVE THE PAGE EDIT VIEWS FROM THE VIEW
        int index = pages.indexOf(p);
        view.removeTab(index);
        
        pages.remove(p);
        
        //IF THERE ARE NO PAGES LEFT AFTER
        if(pages.isEmpty()){
            view.disableRemovePage();
            view.disableExportButton();
            view.disableEditPageTitle();
            view.disableViewSiteButton();
            view.disableEditStudentName();
        }
        view.enableSaveButton();
    }
    
    //CLEARS OUT ALL PAGE DATA IN THIS MODEL AND RESETS THE TITLE TO THE DEFAULT
    public void reset(){
        pages.clear();
        setTitle(DEFAULT_TITLE);
        setStudentName(DEFAULT_STUDENT_NAME);
        //THE VISUAL DATA MUST ALSO BE CLEARED
        view.clearAllTabs();
        view.removeTitle();
        newPageCount = 0;
    }
    
    //THIS METHOD RETURNS WHETHER A PAGE NAME IS ALREADY IN THE PAGE LIST
    public boolean isAlreadyAPage(String pageName){
        for(Page p: pages){
            if(p.getPageTitle().equals(pageName)){
                return false;
            }
        }
        return true;
    }

    /**
     * @return the studentName
     */
    public String getStudentName() {
        return studentName;
    }

    /**
     * @param studentName the studentName to set
     */
    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }
    
}
