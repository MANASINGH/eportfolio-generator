/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportgen.model;

import eportgen.control.ParagraphEditControl;

/**
 *
 * @author Manan Singh
 */
public class ParagraphComponent extends Component{
    
    //THE STRING DATA THAT THE PARAGRAPH WILL CONTAIN
    private String text = "";
    private String font = "";
    
    
    
    public ParagraphComponent(){
        
    }
    
    public ParagraphComponent(String text, String font){
        this.text = text;
        this.font = font;
    }
    
    public ParagraphComponent(String font){
        this.font = font;
    }
    
    //USED IF THE USER WANTS THIS COMPONENT TO BE EDITED. A PARAGRAPHEDITCONTROL DIALOG WILL BE GENERATED
    public void edit(){
        ParagraphEditControl pec = new ParagraphEditControl(this);
        pec.activateEditMode();
        pec.generateDialog();
    }
    
    public void setText(String s){
        text = s;
    }
    
    public String getText(){
        return text;
    }
    
    public String toString(){
        return "Paragraph";
    }

    /**
     * @return the font
     */
    public String getFont() {
        return font;
    }

    /**
     * @param font the font to set
     */
    public void setFont(String font) {
        this.font = font;
    }
}
