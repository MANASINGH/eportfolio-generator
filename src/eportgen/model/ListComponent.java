/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportgen.model;

import eportgen.control.ListEditControl;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Manan Singh
 */
public class ListComponent extends Component {
    
    private ObservableList<String> text = FXCollections.observableArrayList();
    
    public ListComponent(){
        
    }
    
    public void addText(String s){
        text.add(s);
    }
    
    public void remove(){
        
    }
    
    public ObservableList<String> getTextList(){
        return text;
    }
    
    public void setList(ObservableList<String> list){
        text = list;
    }
    
    //USED IF THE USER WANTS THIS COMPONENT TO BE EDITED. A LISTEDITCONTROL DIALOG WILL BE GENERATED
    public void edit(){
        ListEditControl lec = new ListEditControl(this);
        lec.activateEditMode();
        lec.generateDialog();
    }
    
    public String toString(){
        return "List";
    }
}
