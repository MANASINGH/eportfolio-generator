/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportgen.model;

import eportgen.control.VideoEditControl;
import java.io.File;

/**
 *
 * @author Manan Singh
 */
public class VideoComponent extends Component {
    
    private File videoFile;
    private String videoPath;
    private int width;
    private int length;
    private String caption;
    
    public VideoComponent(){
    
    }
    
    public VideoComponent(File videoFile, int width, int length, String caption){
        setVideoFile(videoFile);
        this.width = width;
        this.length = length; 
        this.caption = caption;
    }
    
    public VideoComponent(String videoFile, int width, int length, String caption){
        File file = new File(videoFile);
        if(file.exists()){
            setVideoFile(file);
        }
        this.width = width;
        this.length = length; 
        this.caption = caption;
    }
    
    //USED IF THE USER WANTS THIS COMPONENT TO BE EDITED. A VIDEOEDITCONTROL DIALOG WILL BE GENERATED
    public void edit(){
        VideoEditControl vec = new VideoEditControl(this);
        vec.activateEditMode();
        vec.generateDialog();
    }
    
    public String toString(){
        return "Video";
    }
    
    public String getVideoPath(){
        return videoPath;
    }

    /**
     * @return the videoFile
     */
    public File getVideoFile() {
        return videoFile;
    }

    /**
     * @param videoFile the videoFile to set
     */
    public void setVideoFile(File videoFile) {
        this.videoFile = videoFile;
        String pathName = videoFile.getParent() + "\\";
        this.videoPath = pathName;
    }

    /**
     * @return the width
     */
    public int getWidth() {
        return width;
    }

    /**
     * @param width the width to set
     */
    public void setWidth(int width) {
        this.width = width;
    }

    /**
     * @return the length
     */
    public int getLength() {
        return length;
    }

    /**
     * @param length the length to set
     */
    public void setLength(int length) {
        this.length = length;
    }

    /**
     * @return the caption
     */
    public String getCaption() {
        return caption;
    }

    /**
     * @param caption the caption to set
     */
    public void setCaption(String caption) {
        this.caption = caption;
    }
}
