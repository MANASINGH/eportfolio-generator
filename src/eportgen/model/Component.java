/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportgen.model;

/**
 *
 * @author Manan Singh
 */
public abstract class Component {
    
    protected boolean edited;
    
    public Component(){
        
    }
    
    //ALLOWS THE USER TO EDIT THIS COMPONENT 
    public abstract void edit();
    
    public String toString(){
        return "Component";
    }
    
    public boolean wasEdited(){
        return edited;
    }
    
    public void setEdited(boolean edited){
        this.edited = edited;
    }
}
