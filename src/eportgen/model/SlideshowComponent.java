/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportgen.model;

import SlideshowMaker.Slide;
import eportgen.control.SlideshowEditControl;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Manan Singh
 */
public class SlideshowComponent extends Component {
    
    private ObservableList<Slide> slides = FXCollections.observableArrayList();
    
    //USED IF THE USER WANTS THIS COMPONENT TO BE EDITED. A SLIDESHOWEDITCONTROL DIALOG WILL BE GENERATED
    public void edit(){
        SlideshowEditControl sec = new SlideshowEditControl(this);
        sec.activateEditMode();
        sec.generateDialog();
    }
    
    public String toString(){
        return "Slideshow";
    }

    /**
     * @return the slides
     */
    public ObservableList<Slide> getSlides() {
        return slides;
    }

    /**
     * @param slides the slides to set
     */
    public void setSlides(ObservableList<Slide> slides) {
        this.slides = slides;
    }
}
