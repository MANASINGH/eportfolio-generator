/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportgen.model;

import eportgen.view.PageEditView;
import java.io.File;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *A MODEL FOR AN INDIVIDUAL WEBPAGE (REPRESENTED BY THE TABS IN THE JAVA GUI)
 * @author Manan Singh
 */
public class Page {
    
    //THE VIEW FOR WHICH THIS MODEL IS FOR
    private PageEditView ui;
    
    //BASIC DATA THAT A PAGE HAS
    private String pageTitle;
    private String layoutName = "";
    private String bannerImgPath = "";
    private String bannerImgName = "";
    private String colorName = "";
    private String fontName = "";
    private String footer = "";
    
    //A OBSERVABLE LIST THAT TAKES IN COMPONENTS. THE PAGE EDIT VIEW USES THIS TO DISPLAY WHAT COMPONENTS ARE ON THE WEBPAGE AT THE CURRENT TIME
    ObservableList<Component> components = FXCollections.observableArrayList();
    
    
    public Page(){
        //WHEN A NEW PAGE IS MADE, THE DEFAULT TITLE IS "NEW PAGE"
        setPageTitle("New Page");
    }
    
    public void checkIfBannerExists(){
        File file = new File(bannerImgPath + bannerImgName);
        if(!file.exists()){
            bannerImgPath = "";
            bannerImgName = "";
        }
    }
    
    //RETURNS A LIST OF ALL THE IMG COMPONENTS OF IN THE PAGE
    public ArrayList<ImgComponent> getAllImages(){
        ArrayList<ImgComponent> images = new ArrayList<ImgComponent>();
        for(Component c: components){
            if(c.toString().equals("Image")){
                images.add((ImgComponent) c);
            }
        }
        return images;
    }
    
    //RETURNS A LIST OF ALL THE VIDEO COMPONENTS OF IN THE PAGE
    public ArrayList<VideoComponent> getAllVideos(){
        ArrayList<VideoComponent> videos = new ArrayList<VideoComponent>();
        for(Component c: components){
            if(c.toString().equals("Video")){
                videos.add((VideoComponent) c);
            }
        }
        return videos;
    }
    
    public ArrayList<SlideshowComponent> getAllSlideshows(){
        ArrayList<SlideshowComponent> slideshows = new ArrayList<SlideshowComponent>();
        for(Component c: components){
            if(c.toString().equals("Slideshow")){
                slideshows.add((SlideshowComponent) c);
            }
        }
        return slideshows;
    }
    
    //ADDS A COMPONENT TO THIS PAGE
    public void addComponent(Component c){
        components.add(c);
    }

    /**
     * @return the layoutName
     */
    public String getLayoutName() {
        return layoutName;
    }

    /**
     * @param layoutName the layoutName to set
     */
    public void setLayoutName(String layoutName) {
        this.layoutName = layoutName;
    }

    /**
     * @return the bannerImgName
     */
    public String getBannerImgPath() {
        return bannerImgPath;
    }

    /**
     * @param bannerImgName the bannerImgName to set
     */
    public void setBannerImgPath(String bannerImgName) {
        this.bannerImgPath = bannerImgName;
    }

    /**
     * @return the colorName
     */
    public String getColorName() {
        return colorName;
    }

    /**
     * @param colorName the colorName to set
     */
    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    /**
     * @return the fontName
     */
    public String getFontName() {
        return fontName;
    }

    /**
     * @param fontName the fontName to set
     */
    public void setFontName(String fontName) {
        this.fontName = fontName;
    }

    /**
     * @return the pageTitle
     */
    public String getPageTitle() {
        return pageTitle;
    }

    /**
     * @param pageTitle the pageTitle to set
     */
    public void setPageTitle(String pageTitle) {
        this.pageTitle = pageTitle;
    }
    
    public ObservableList<Component> getComponents(){
        return components;
    }

    /**
     * @return the bannerImgName
     */
    public String getBannerImgName() {
        return bannerImgName;
    }

    /**
     * @param bannerImgName the bannerImgName to set
     */
    public void setBannerImgName(String bannerImgName) {
        this.bannerImgName = bannerImgName;
    }

    /**
     * @return the footer
     */
    public String getFooter() {
        return footer;
    }

    /**
     * @param footer the footer to set
     */
    public void setFooter(String footer) {
        this.footer = footer;
    }
}
