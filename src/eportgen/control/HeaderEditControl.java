/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportgen.control;

import static eportgen.StartupConstants.UI_CSS;
import eportgen.model.HeaderComponent;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author Manan Singh
 */
public class HeaderEditControl extends ComponentEditControl{
    
    private boolean editMode = false;
    private TextField headerString = new TextField();
    
    public HeaderEditControl(HeaderComponent he){
        super(he);
        headerString.setText(he.getText());
    }
    
    public void apply(){
        //COMPARES THE COMPONENT DATA AND THIS CONTROLS DATA, IF THEY'RE THE SAME, THEN IT HASN'T BEEN EDITED
        if(!headerString.getText().equals("")){
            if(!headerString.getText().equals(( (HeaderComponent) component ).getText())){
                ( (HeaderComponent) component ).setEdited(true);
            }
            
            ( (HeaderComponent) component ).setText(headerString.getText());
            
        }
    }
    
    public void generateDialog(){
        Stage stage = new Stage();
        if(!editMode){
            stage.setTitle("Create header component");
        }else{
            stage.setTitle("Edit a header component");
        }
        stage.initModality(Modality.APPLICATION_MODAL);
        VBox root = new VBox();
        Scene scene = new Scene(root, 500, 100);
        scene.getStylesheets().add(UI_CSS);
        
        root.getStyleClass().add("componentEditView-style");
        
        Text prompt = new Text("Input your header:");
        
        Button ok = new Button("OK");
        ok.setOnAction((ActionEvent e) -> {
            apply();
            stage.close();
        });
        
        root.getChildren().addAll(prompt, headerString, ok);
        
        root.setSpacing(2);
        root.setPadding(new Insets(5, 5, 5, 5));
        root.setAlignment(Pos.CENTER);
        
        stage.setScene(scene);
        stage.showAndWait();
    }
    
    public void activateEditMode(){
        editMode = true;
    }
}
