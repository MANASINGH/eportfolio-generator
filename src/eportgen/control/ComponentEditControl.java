package eportgen.control;

import eportgen.model.Component;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *THIS CLASS DOUBLES AS BOTH A CONTROL AND VIEW FOR THE COMPONENT CLASS
 * @author Manan Singh
 */
public abstract class ComponentEditControl {
    
    protected Component component;
    
    public ComponentEditControl(Component component){
        this.component = component;
    }
    
    //APPLY ANY CHANGES THAT THE USER MAY HAVE DONE THROUGH THE GUI DIALOG TO THE COMPONENT AND CREATE A NEW OBJECT 
    //THAT WILL REPLACE THE OLD ON WITH THE UPDATED INFORMATION
    public abstract void apply();
    
    //WILL MAKE THE DIALOG THAT CONTROLS THE COMPONENT FOR THIS CLASS APPEAR SO THAT THE USER CAN EDIT THE COMPONENT
    public abstract void generateDialog();
}
