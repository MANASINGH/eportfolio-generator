/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportgen.control;

import Dialogs.AlertDialog;
import static eportgen.StartupConstants.IMAGES_PATH;
import static eportgen.StartupConstants.UI_CSS;
import eportgen.model.ImgComponent;
import java.io.File;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author Manan Singh
 */
public class ImgEditControl extends ComponentEditControl {
    
    private Text imageFileName = new Text("Image File: ");
    private TextField lengthField = new TextField();
    private TextField widthField = new TextField();
    private TextField captionField = new TextField();
    
    private File chosenFile;
    
    private ToggleGroup radioGroup = new ToggleGroup();
    private RadioButton floatLeft = new RadioButton("Float Left");
    private RadioButton neither = new RadioButton("Neither");
    private RadioButton floatRight = new RadioButton("Float Right");
    
    private boolean editMode = false;
    
    public ImgEditControl(ImgComponent ic){
        super(ic);
        if(ic.getImageFile() != null){
            imageFileName.setText("Image File: " + ic.getImageFile().getName());
            chosenFile = ic.getImageFile();
        }else{
            imageFileName.setText("Image File: " );
        }
        lengthField.setText(Integer.toString(ic.getLength()));
        widthField.setText(Integer.toString(ic.getWidth()));
        captionField.setText(ic.getCaption());
        chooseFloating(ic.getFloating());
    }
    
    //THIS FUNCTION TELLS YOU IF THE COMPONENT'S DATA IS THE EXACT SAME AS THE DATA THAT THIS EDIT CONTROL HAS IN IT. IT IS USED TO 
    //TELL IF THE COMPONENT HAS BEEN EDITED OR NOT
    private boolean isSameAsCurrentComponent(){
        ImgComponent c = ((ImgComponent) component);
        if(chosenFile == (c.getImageFile())){
            int width = Integer.parseInt(widthField.getText());
            int length = Integer.parseInt(lengthField.getText());
            if(width == c.getWidth() && length == c.getLength()){
                if(captionField.getText().equals(c.getCaption())){
                    if(getFloatingNum() == c.getFloating()){
                        //THIS MEANS THAT THE CONTROL AND COMPONENT HAVE THE SAME DATA
                        return true;
                    }
                }
            }
        }
        //THIS MEANS THAT THE CONTROL AND THE COMPONENT HAVE DIFFERENT DATA
        return false;
    }
    
    private void chooseFloating(int floatType){
        if(floatType == 1){
            floatLeft.setSelected(true);
        }else if(floatType == 2){
            neither.setSelected(true);
        }else if(floatType == 3){
            floatRight.setSelected(true);
        }else{
            //ERROR HANDLING?
        }
    }
    
    private int getFloatingNum(){
        if(floatLeft.isSelected()){
            return 1;
        }else if(neither.isSelected()){
            return 2;
        }else if(floatRight.isSelected()){
            return 3;
        }else{
            return 4;
        }
    }
    
    public void apply(){
        
        if(!isSameAsCurrentComponent()){
            ((ImgComponent) component).setEdited(true);
        }
        
        ((ImgComponent) component).setImageFile(chosenFile);
        ((ImgComponent) component).setWidth(Integer.parseInt(widthField.getText()));
        ((ImgComponent) component).setLength(Integer.parseInt(lengthField.getText()));
        ((ImgComponent) component).setCaption(captionField.getText());
        ((ImgComponent) component).setFloating(getFloatingNum());
    }
    
    public void generateDialog(){
        Stage stage = new Stage();
        if(!editMode){
            stage.setTitle("Create image component");
        }else{
            stage.setTitle("Edit an image component");
        }
        stage.initModality(Modality.APPLICATION_MODAL);
        VBox root = new VBox();
        Scene scene = new Scene(root, 500, 500);
        scene.getStylesheets().add(UI_CSS);
        
        root.getStyleClass().add("componentEditView-style");
        
        Button ok = new Button("OK");
        
        ok.setOnAction((ActionEvent e) ->{
            if(chosenFile != null){
                apply();
                stage.close();
            }else{
                AlertDialog.alert("You must choose a valid input image file!");
            }
        });
        
        
        Button chooseImageFile = new Button("Choose image file");
        
        chooseImageFile.setOnAction((ActionEvent e) -> {
            selectImage();
        });
        
        Text sizePrompt = new Text("Enter length and width respectively in the following fields: ");
        Text captionPrompt = new Text("Enter the caption: ");
        
        lengthField.setPromptText("Enter the preferred length of the picture");
        widthField.setPromptText("Enter the preferred width of the picture");
        captionField.setPromptText("Enter the caption");
        
        VBox radioButtons = new VBox();
        radioButtons.getChildren().addAll(floatLeft, neither, floatRight);
        
        floatLeft.setToggleGroup(radioGroup);
        neither.setToggleGroup(radioGroup);
        floatRight.setToggleGroup(radioGroup);
        
        root.setSpacing(10);
        root.setPadding(new Insets(10, 10, 10, 10));
        root.setAlignment(Pos.CENTER);
        
        root.getChildren().addAll(imageFileName, chooseImageFile, captionPrompt, 
                captionField, sizePrompt, lengthField, widthField, radioButtons, ok);
        
        stage.setScene(scene);
        stage.showAndWait();
    }
    
    public void activateEditMode(){
        editMode = true;
    }
    
    private void selectImage(){
        FileChooser imageFileChooser = new FileChooser();
	
	// SET THE STARTING DIRECTORY
	imageFileChooser.setInitialDirectory(new File(IMAGES_PATH));
	
	// LET'S ONLY SEE IMAGE FILES
	FileChooser.ExtensionFilter jpgFilter = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.JPG");
	FileChooser.ExtensionFilter pngFilter = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.PNG");
	FileChooser.ExtensionFilter gifFilter = new FileChooser.ExtensionFilter("GIF files (*.gif)", "*.GIF");
	imageFileChooser.getExtensionFilters().addAll(jpgFilter, pngFilter, gifFilter);
	
	// LET'S OPEN THE FILE CHOOSER
	File file = imageFileChooser.showOpenDialog(null);
	if (file != null) {
	    String path = file.getPath().substring(0, file.getPath().indexOf(file.getName()));
	    String fileName = file.getName();
	    chosenFile = file;
            imageFileName.setText("Image File: " + fileName);
	}	    
	else {
            AlertDialog.alert("You didn't select a valid image file!");
            
	}
    }
}
