/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportgen.control;

import Dialogs.AlertDialog;
import SlideshowMaker.Slide;
import SlideshowMaker.SlideEditView;
import static eportgen.StartupConstants.UI_CSS;
import eportgen.model.SlideshowComponent;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author Manan Singh
 */
public class SlideshowEditControl extends ComponentEditControl{
    
    private boolean editMode = false;
    //THIS WILL KEEP TRACK OF ALL THE 
    private ObservableList<Slide> slides;
    
    public SlideshowEditControl(SlideshowComponent sc){
        super(sc);
        slides = FXCollections.observableArrayList();
        for(Slide s: sc.getSlides()){
            slides.add(s.getCopy());
        }
    }
    
    public void apply(){
        ((SlideshowComponent) component).setSlides(slides);
        ((SlideshowComponent) component).setEdited(true);
    }
    
    public void generateDialog(){
        new SlideshowMakerView();
    }
    
    private class SlideshowMakerView extends Stage {
        
        private VBox slideArea = new VBox();
        private ArrayList images = new ArrayList();
        private ArrayList captions = new ArrayList();
        private BorderPane root = new BorderPane();
        private HBox menuBar = new HBox();
        
        private ObservableList<Slide> tempSlides = FXCollections.observableArrayList();
        
        Button addSlide = new Button("Add Slide");
        Button removeSlide = new Button("Remove Slide");
        Button slideUp = new Button("Move Slide Up");
        Button slideDown = new Button("Move Slide Down");
        Button ok = new Button("OK");
        
        
        public SlideshowMakerView(){
            this.initModality(Modality.APPLICATION_MODAL); 
            if(!editMode){
                this.setTitle("Create Slideshow component");
            }else{
                this.setTitle("Edit a Slideshow component");
            }
            
            //ADD ALL THE COPIED SLIDES
            for(Slide s: slides){
                addSlide(s);
            }
            
            ScrollPane scroller = new ScrollPane(root);
            Scene scene = new Scene(scroller, 550, 700);
            scene.getStylesheets().add(UI_CSS);
            
            slideArea.setSpacing(2);
            slideArea.setPadding(new Insets(5, 5, 5, 5));
            
            menuBar.setSpacing(10);
            menuBar.setPadding(new Insets(10, 10, 10, 10));
            menuBar.getStyleClass().add("default-style");
            menuBar.getChildren().addAll(addSlide, removeSlide, slideUp, slideDown, ok);
            root.setTop(menuBar);
            
            root.setCenter(slideArea);
            
            addSlide.setOnAction(new EventHandler<ActionEvent>(){
                public void handle(ActionEvent e){
                    Slide s = new Slide();
                    SlideEditView sev = new SlideEditView(s);
                    //ADD IT TO THE EDIT COMONENTS COPY OF SLIDES
                    slides.add(s);
                    sev.getStyleClass().add("slide");
                    slideArea.getChildren().add(sev);
                    
                }
            });
            
            removeSlide.setOnAction(new EventHandler<ActionEvent>(){
                public void handle(ActionEvent e){
                    //REMOVE THE SLIDE FROM THE EDIT COMPONENTS COPY OF SLIDES
                    try {
                        if((Slide)SlideEditView.selected.getSlide() != null){
                        slides.remove((Slide)SlideEditView.selected.getSlide());
                        slideArea.getChildren().remove(SlideEditView.selected);
                        SlideEditView.nullify();
                        }
                    }catch(NullPointerException ex){
                        
                    }
                    
                    
                    
                }
            });
            
            slideUp.setOnAction(new EventHandler<ActionEvent>(){
                public void handle(ActionEvent e){
                    int thisIndex = slideArea.getChildren().indexOf(SlideEditView.selected);
                    int prevIndex = thisIndex - 1;
                    if(prevIndex >= 0){
                        SlideEditView prevSev = (SlideEditView)slideArea.getChildren().get(prevIndex);
                        
                        try{
                            slideArea.getChildren().remove(prevIndex);
                            slideArea.getChildren().add(thisIndex, prevSev);
                            //DO THE SAME THING TO THE COPIES
                            slides.remove(prevIndex);
                            slides.add(thisIndex, prevSev.getSlide());
                        }catch(NullPointerException ex){
                            
                        }
                    }else{
                        
                        try{
                            slideArea.getChildren().remove(SlideEditView.selected);
                            slideArea.getChildren().add(slideArea.getChildren().size(), SlideEditView.selected);
                            //DO THE SAME THING TO THE COPIES
                            slides.remove(SlideEditView.selected.getSlide());
                            slides.add(slides.size(), SlideEditView.selected.getSlide());
                        }catch(NullPointerException ex){
                            
                        }
                    }
                    
                }
            });
            
            slideDown.setOnAction(new EventHandler<ActionEvent>(){
                public void handle(ActionEvent e){
                    if(SlideEditView.selected != null){
                        int thisIndex = slideArea.getChildren().indexOf(SlideEditView.selected);
                        int nextIndex = thisIndex + 1;
                        if(nextIndex < slideArea.getChildren().size()){
                            SlideEditView nextSev = (SlideEditView)slideArea.getChildren().get(nextIndex);
                            slideArea.getChildren().remove(nextIndex);
                            slideArea.getChildren().add(thisIndex, nextSev);
                            //DO THE SAME THING TO THE COPIES
                            slides.remove(nextIndex);
                            slides.add(thisIndex, nextSev.getSlide());
                        }else{
                            slideArea.getChildren().remove(SlideEditView.selected);
                            slideArea.getChildren().add(0, SlideEditView.selected);
                            //DO THE SAME THING TO THE COPIES
                            slides.remove(SlideEditView.selected.getSlide());
                            slides.add(0, SlideEditView.selected.getSlide());
                        }
                    }
                    
                }
            });
            
            ok.setOnAction((ActionEvent e) -> {
                if(!slideArea.getChildren().isEmpty()){
                    apply();
                }else{
                    AlertDialog.alert("Slideshow cannot contain zero slides!");
                }
                this.close();
            });
            
            this.setScene(scene);
            this.showAndWait();
        }
        
        private void addSlide(Slide s){
            SlideEditView sev = new SlideEditView(s);
            sev.updateSlideImage();
            sev.setCaptionText(s.getCaption());
            sev.getStyleClass().add("slide");
            slideArea.getChildren().add(sev);
        }
    }
    
    public void activateEditMode(){
        editMode = true;
    }
    
}
