/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportgen.control;

import Dialogs.PageTitleChooser;
import Dialogs.StudentNameChooser;
import eportgen.model.EPortModel;
import eportgen.model.Page;
import eportgen.view.EPortView;

/**
 *HANDLES EVENTS DEALING WITH THE PAGES OF THE WEBSITE (THE TABS ON THE JAVA GUI)
 * @author Manan Singh
 */
public class PagesControl {
    
    private static EPortView view;
    
    public PagesControl(EPortView view){
        this.view = view;
    }
    
    public void processAddPageRequest(){
        EPortModel model = view.getModel();
        model.addNewPage();
    }
    
    public void processRemovePageRequest(){
        EPortModel model = view.getModel();
        //HAVE TO CALL REMOVE PAGE()
        Page p = view.getSelectedPage();
        model.removePage(p);
    }
    
    public void processSetPageTitleRequest(){
        EPortModel model = view.getModel();
        Page currentPage = view.getSelectedPage();
        String currentTitle = currentPage.getPageTitle();
        PageTitleChooser ptc = new PageTitleChooser(view.getModel(), currentPage);
        ptc.createDialog();
        if(!currentTitle.equals(currentPage.getPageTitle())){
            view.enableSaveButton();
            //HAVE TO EDIT THE TITLE ON THE TAB VISUALLY
            view.getSelctedTab().setText(currentPage.getPageTitle());
        }
    }
    
    public void processNewStudentNameRequest(){
        EPortModel model = view.getModel();
        String currentName = model.getStudentName();
        StudentNameChooser snc = new StudentNameChooser(model);
        snc.createDialog();
        if(!currentName.equals(model.getStudentName())){
            view.enableSaveButton();
            //HAVE TO EDIT THE STUDENT NAME VISUALLY
            view.removeTitle();
            view.addTitle();
        }
    }
    
    //THE VIEW WAS MADE STATIC SO THAT THIS FUNCTION COULD EXIST FOR COMPONENTS THAT WANTED TO ENABLE SAVE BUTTONS BECAUSE A CHANGE WAS MADE
    public static void signalChange(){
        view.enableSaveButton();
    }
}
