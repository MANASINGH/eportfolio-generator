/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportgen.control;

import Dialogs.FontChooser;
import static eportgen.StartupConstants.UI_CSS;
import eportgen.model.ParagraphComponent;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author Manan Singh
 */
public class ParagraphEditControl extends ComponentEditControl{
    
    // USED TO SET TITLE OF THE STAGE TO DETERMINE IF EDITING OR CREATING
    private boolean editMode = false;
    //AREA WHERE THE USER INPUTS THE TEXT FOR THE PARAGRAPH
    private TextArea textInput = new TextArea();
    //DISPLAYS FONT
    private Text theFont = new Text();
    //FOR CONFIRMING CHANGES
    private Button ok = new Button("OK");
    //FOR CHOOSING FONTS
    private FontChooser fc = new FontChooser(theFont);
    
    
    
    public ParagraphEditControl(ParagraphComponent pc){
        super(pc);
        //READ THE DATA FROM THE PARAGRAPH COMPONENT
        textInput.setText(pc.getText());
        if(pc.getFont().equals("")){
            updateFont();
        }else{
            fc.selectFont(pc.getFont());
            updateFont();
        }
    }
    
    public ParagraphEditControl(){ 
        super(new ParagraphComponent());
    }
    
    //APPLIES THE CHANGES TO THE OBJECT WHEN 'OK' BUTTON IS PRESSED AND CLOSES THE 
    public void apply(){
        if(!isSameAsComponent()){ 
         ((ParagraphComponent) component).setEdited(true);
        }
        ((ParagraphComponent) component).setText(textInput.getText());
    }
    
    private boolean isSameAsComponent(){
        if(!((ParagraphComponent) component).getFont().equals(theFont.getText())){
            if(((ParagraphComponent) component).getText().equals(textInput.getText())){
                return true;
        }
            return false;
        }else{
            return false;
        }
        
    }
    
    //GENERATE THE DIALOG SO THAT THE USER CAN EDIT A PARAGRAPHCOMPONENT
    public void generateDialog(){
        Stage stage = new Stage();
        if(!editMode){
            stage.setTitle("Create a paragraph component");
        }else{
            stage.setTitle("Edit a paragraph component");
        }
        stage.initModality(Modality.APPLICATION_MODAL);
        VBox root = new VBox();
        Scene scene = new Scene(root, 500, 500);
        scene.getStylesheets().add(UI_CSS);
        
        root.getStyleClass().add("componentEditView-style");
        
        Text text = new Text("Put in whatever text you would like for your paragraph in the space below:");
        text.setWrappingWidth(400);
        
        VBox hyperlinkSection = new VBox();
        hyperlinkSection.setSpacing(5);
        
        Button selectFont = new Button("Select Font");
        
        selectFont.setOnAction((ActionEvent e) -> {
            fc.createDialog();
            ((ParagraphComponent) component).setFont(fc.getCurrentFont());
        });
        
        TextField hyperlinkText = new TextField();
        TextField hyperlinkLink = new TextField();
        Button addHyperlink = new Button("Add hyperlink");
        hyperlinkText.setPromptText("The text that the hyperlink will display");
        hyperlinkLink.setPromptText("The link that the hyperlink will goes to");
        
        root.setPadding(new Insets(10, 10, 10, 10));
        root.setAlignment(Pos.CENTER);
        root.setSpacing(10);
        
        ok.setOnAction((ActionEvent e) -> {
            apply();
            stage.close();
        });
        
        hyperlinkSection.getChildren().addAll(hyperlinkText, hyperlinkLink, addHyperlink);
        
        root.getChildren().add(text);
        root.getChildren().add(textInput);
        root.getChildren().add(selectFont);
        root.getChildren().add(theFont);
        root.getChildren().add(hyperlinkSection);
        root.getChildren().add(ok);
        stage.setScene(scene);
        
        stage.showAndWait();
    }
    
    public String toString(){
        return "Paragraph";
    }
    
    public void updateFont(){
        ((ParagraphComponent) component).setFont(fc.getCurrentFont());
    }
    
    public void activateEditMode(){
        editMode = true;
    }
}
