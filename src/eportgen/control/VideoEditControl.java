/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportgen.control;

import Dialogs.AlertDialog;
import static eportgen.StartupConstants.IMAGES_PATH;
import static eportgen.StartupConstants.UI_CSS;
import eportgen.model.ImgComponent;
import eportgen.model.VideoComponent;
import java.io.File;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author Manan Singh
 */
public class VideoEditControl extends ComponentEditControl {
    
    private Text videoFileName = new Text("Video File: ");
    
    private File chosenFile;
    
    private TextField lengthField = new TextField();
    private TextField widthField = new TextField();
    private TextField captionField = new TextField();
    private boolean editMode = false;
    
    public VideoEditControl(VideoComponent vc){
        super(vc);
        if(vc.getVideoFile() != null){
            videoFileName.setText("Video File: " + vc.getVideoFile().getName());
            chosenFile = vc.getVideoFile();
        }else{
            videoFileName.setText("Video File: ");
        }
        lengthField.setText(Integer.toString(vc.getLength()));
        widthField.setText(Integer.toString(vc.getWidth()));
        captionField.setText(vc.getCaption());
    }
    
    public void apply(){
        if(!isSameAsCurrentComponent()){
            ((VideoComponent) component).setEdited(true);
        }
        
        ((VideoComponent) component).setVideoFile(chosenFile);
        ((VideoComponent) component).setWidth(Integer.parseInt(widthField.getText()));
        ((VideoComponent) component).setLength(Integer.parseInt(lengthField.getText()));
        ((VideoComponent) component).setCaption(captionField.getText());
    }
    
    public void generateDialog(){
        Stage stage = new Stage();
        if(!editMode){
            stage.setTitle("Create video component");
        }else{
            stage.setTitle("Edit a video component");
        }
        stage.initModality(Modality.APPLICATION_MODAL);
        VBox root = new VBox();
        Scene scene = new Scene(root, 500, 500);
        scene.getStylesheets().add(UI_CSS);
        
        root.getStyleClass().add("componentEditView-style");
        
        
        Button ok = new Button("OK");
        ok.setOnAction((ActionEvent e) ->{
            if(chosenFile != null){
                apply();
                stage.close();
            }else{
                AlertDialog.alert("You must choose a valid input video file!");
            }
        });
        
        Button chooseVideoFile = new Button("Choose video file");
        chooseVideoFile.setOnAction((ActionEvent e) -> {
            selectVideo();
        });
        
        Text sizePrompt = new Text("Enter length and width respectively in the following fields: ");
        Text captionPrompt = new Text("Enter the caption: ");
        
        lengthField.setPromptText("Enter the preferred length of the picture");
        widthField.setPromptText("Enter the preferred width of the picture");
        captionField.setPromptText("Enter the caption for this video");
        
        root.setSpacing(10);
        root.setPadding(new Insets(10, 10, 10, 10));
        root.setAlignment(Pos.CENTER);
        
        root.getChildren().addAll(videoFileName, chooseVideoFile, captionPrompt, 
                captionField, sizePrompt, lengthField, widthField, ok);
        
        stage.setScene(scene);
        stage.showAndWait();
    }
    
    //THIS FUNCTION TELLS YOU IF THE COMPONENT'S DATA IS THE EXACT SAME AS THE DATA THAT THIS EDIT CONTROL HAS IN IT. IT IS USED TO 
    //TELL IF THE COMPONENT HAS BEEN EDITED OR NOT
    private boolean isSameAsCurrentComponent(){
        VideoComponent c = ((VideoComponent) component);
        if(chosenFile == (c.getVideoFile())){
            int width = Integer.parseInt(widthField.getText());
            int length = Integer.parseInt(lengthField.getText());
            if(width == c.getWidth() && length == c.getLength()){
                if(captionField.getText().equals(c.getCaption())){
                    //THIS MEANS THAT THE CONTROL AND COMPONENT HAVE THE SAME DATA
                    return true;
                }
            }
        }
        //THIS MEANS THAT THE CONTROL AND THE COMPONENT HAVE DIFFERENT DATA
        return false;
    }
    
    private void selectVideo(){
        FileChooser imageFileChooser = new FileChooser();
	
	// LET'S ONLY SEE IMAGE FILES
	FileChooser.ExtensionFilter mp4Filter = new FileChooser.ExtensionFilter("MP4 files (*.mp4)", "*.MP4");
	imageFileChooser.getExtensionFilters().addAll(mp4Filter);
	
	// LET'S OPEN THE FILE CHOOSER
	File file = imageFileChooser.showOpenDialog(null);
	if (file != null) {
	    String path = file.getPath().substring(0, file.getPath().indexOf(file.getName()));
	    String fileName = file.getName();
	    chosenFile = file;
            videoFileName.setText("Video File: " + fileName);
	}	    
	else {
            AlertDialog.alert("You didn't select a valid image file!");
            
	}
    }
    
    public void activateEditMode(){
        editMode = true;
    }
}
