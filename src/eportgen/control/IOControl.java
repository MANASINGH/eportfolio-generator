/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportgen.control;

import Dialogs.AlertDialog;
import Dialogs.SaveAsFilePrompt;
import Dialogs.SaveFilePrompt;
import static eportgen.StartupConstants.SAVED_PORTFOLIOS;
import eportgen.file.FileOperator;
import eportgen.model.Component;
import eportgen.model.EPortModel;
import eportgen.model.Page;
import eportgen.view.EPortView;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.stage.DirectoryChooser;

/**
 *A CLASS USED TO HANDLE EVENTS FOR ALL OF THE IO OPERATIONS (OPEN, SAVE, SAVEAS, ETC.) AND FOR SWITCHING THE 
 *WORKSPACE VIEW BETWEEN PAGE EDITOR AND WEBSITE VIEWER
 * @author Manan Singh
 */
public class IOControl {
    
    //THE VIEW THAT CONTAINS THE BUTTONS THAT USE THE IO OPERATIONS PROVIDED BY THIS CLASS
    private EPortView view;
    //KEEPS TRACK OF IF THE EPORTFOLIO IS SAVED
    private boolean saved = false;
    //THE OBJECT THAT DOES ALL THE SAVING AND LOADING OF EPORTFOLIOS
    private FileOperator fo;
    //KEEPS TRACK OF IF THE USER HAS CHANGED THE TITLE OF THE EPORTFOLIO AND SAVED WORK. IF FALSE, THE USER ENTERS IN THE NEW TITLE OF THE PORTFOLIO
    private boolean firstTimeSaving = true;
    
    public IOControl(EPortView view){
        this.view = view;
    }
    
    //HANDLES THE EVENT WHERE THE USER WANTS TO MAKE A NEW PORTFOLIO
    public void processNewPortfolioRequest(){
        boolean saveWork = true;
        
        //PROMPT TO SAVE THE CURRENT PORTFOLIO IF WORK IS NOT ALREADY SAVED
        if(!saved){
            saveWork = promptToSave();
        }
        
        if(saveWork){
            //CLEAR OUT THE DATA FOR ANY CURRENT PORTFOLIO
            EPortModel model = view.getModel();
            model.reset();
        
            //ENABLE APPROPRIATE BUTTONS
            view.enableAddPage();
        
            view.addTitle();
            
            view.disableSaveButton();
            view.disableExportButton();
            view.disableEditPageButton();
            view.disableViewSiteButton();
            view.disableRemovePage();
            view.disableEditStudentName();
            view.disableEditPageTitle();
            
            
            firstTimeSaving = true;
        }
    }
    
    public void processLoadRequest(){
        boolean continueToOpen = true;
        if(!saved){
            //THE USER SHOULD BE ABLE TO OPT OUT HERE WITH A CANCEL
            continueToOpen = promptToSave();
        }
        //IF THE USER REALLY WANTS TO OPEN UP A NEW PORTFOLIO
        if(continueToOpen){
            promptToOpen();
        }
    }
    
    public void proecessExitRequest(){
        boolean continueToExit = true;
        if(!saved){
            //THE USER SHOULD BE ABLE TO OPT OUT HERE WITH A CANCEL
            continueToExit = promptToSave();
        }
        //IF THE USER REALLY WANTS TO OPEN UP A NEW PORTFOLIO
        if(continueToExit){
            view.getWindow().close();
        }
    }
    
    public void processSaveRequest(){
        
        //IF THE USER HAS NOT SAVED FOR THE FIRST TIME YET, THEN PROMPT THE USER TO ENTER TEXT
        if(firstTimeSaving){
            boolean savedAsName = promptToSaveAs();
        }else{
            fo = new FileOperator(view.getModel());
            try {
                fo.save();
            } catch (IOException ex) {
                Logger.getLogger(IOControl.class.getName()).log(Level.SEVERE, null, ex);
            }
            view.disableSaveButton(); //ALSO SETS SAVED TO TRUE
            view.removeTitle();
            view.addTitle();
            
            //MARK ALL THE COMPONENTS AS UNEDITED NOW THAT THEY ARE SAVED
            for(Page p: view.getModel().getPages()){
                for(Component c: p.getComponents()){
                    c.setEdited(false);
                }
            }
        }
    }
    
    public void processExportRequest(){
        processSaveRequest();
        if(saved){
            fo = new FileOperator(view.getModel());
            fo.export();
        }
        
    }
    
    public void processViewSiteRequest(){
        processSaveRequest();
        if(saved){
            fo = new FileOperator(view.getModel());
            fo.exportToView();
            view.switchToSiteView();
            view.enableEditPageButton();
            view.disableViewSiteButton();
            view.disableSaveAsButton();
            view.disableAddPage();
            view.disableRemovePage();
            view.disableExportButton();
            view.disableSaveButton();
            view.disableEditPageTitle();
            view.disableEditStudentName();
            view.disableNewPortfolio();
            view.disableOpenPortfolio();
        }
        
    }
    
    public boolean promptToSave(){
        SaveFilePrompt saveDialog = new SaveFilePrompt();
        // PROMPT THE USER TO SAVE UNSAVED WORK
        boolean saveWork = saveDialog.execute(); // @todo change this to prompt

        // IF THE USER SAID YES, THEN SAVE BEFORE MOVING ON
        if (saveWork) {
            
           processSaveRequest();
           return !firstTimeSaving; //DONT ASK WHY, IT JUST WORKS. BELIEVE ME (ITS FOR THE CASE WHERE YOU X OUT OF THE SELECTING THE TITLE AFTER YOU 
           //SAY YES TO SAVING IN PROCESSNEWPORTFOLIOREQUEST
            
        } // IF THE USER SAID CANCEL, THEN WE'LL TELL WHOEVER
          // CALLED THIS THAT THE USER IS NOT INTERESTED ANYMORE
        else if (saveDialog.wasCancelled()) {
            saveDialog.refresh();
            return false;
        }
        
        saveDialog.refresh();
        // IF THE USER SAID NO, WE JUST GO ON WITHOUT SAVING
        // BUT FOR BOTH YES AND NO WE DO WHATEVER THE USER
        // HAD IN MIND IN THE FIRST PLACE
        return true;
    }
    
    //ASKS THE USER HOW WHAT THE TITLE OF THE EPORTFOLIO SHOULD BE
    public boolean promptToSaveAs(){
        SaveAsFilePrompt saveAs = new SaveAsFilePrompt(view.getModel());
        
        boolean selectedName = SaveAsFilePrompt.execute();
        if(selectedName){ 
            fo = new FileOperator(view.getModel());
            try {
                fo.save();
            } catch (IOException ex) {
                Logger.getLogger(IOControl.class.getName()).log(Level.SEVERE, null, ex);
            }
            view.disableSaveButton();
            view.removeTitle();
            view.addTitle();
            firstTimeSaving = false;
            
        }else if(saveAs.wasCancelled()){
            saveAs.refresh();
            return false;
        }
        
        saveAs.refresh();
        return true;
    }
    
    public void promptToOpen(){
        File f = new File(SAVED_PORTFOLIOS);
        if(!f.exists()){
            f.mkdir();
        }
        
        // AND NOW ASK THE USER FOR THE COURSE TO OPEN
        DirectoryChooser portfolioChooser = new DirectoryChooser();
        portfolioChooser.setInitialDirectory(new File(SAVED_PORTFOLIOS));
        portfolioChooser.setTitle("Select any ePortfolios listed below");
        File selectedFile = portfolioChooser.showDialog(view.getWindow());
        

        // ONLY OPEN A NEW FILE IF THE USER SAYS OK
        if (selectedFile != null) {
            try {
		EPortModel model = view.getModel();
                fo = new FileOperator(model);
                
                //LOAD THE SLIDESHOW
                fo.load(selectedFile);
                
                //RELOAD THE PORTFOLIO
                //model.reset();
                //view.clearAllTabs();
                //view.removeTitle();
                //view.addTitle();
                
                saved = true;
                
                //ENABLE/DISABLE APPROPRIATE CONTROLS
                view.disableSaveButton();
                view.addTitle();
                firstTimeSaving = false;
                
            } catch (Exception e) {
               AlertDialog.alert("There was an error in loading the file.");
            }
        }
    }

    /**
     * @return the saved
     */
    public boolean isSaved() {
        return saved;
    }

    /**
     * @param saved the saved to set
     */
    public void setSaved(boolean saved) {
        this.saved = saved;
    }

    /**
     * @return the savedAs
     */
    public boolean isFirstTimeSaving() {
        return firstTimeSaving;
    }

    /**
     * @param savedAs the savedAs to set
     */
    public void setFirstTimeSaving(boolean savedAs) {
        this.firstTimeSaving = savedAs;
    }
    
    
}
