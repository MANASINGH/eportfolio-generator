/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportgen.control;

import static eportgen.StartupConstants.UI_CSS;
import eportgen.model.ListComponent;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author Manan Singh
 */
public class ListEditControl extends ComponentEditControl {
    
    private boolean editMode = false;
    private TextArea inputArea = new TextArea();
    ListView<String> list = new ListView<String>();
    private ObservableList<String> listData;
    
    public ListEditControl(ListComponent lc){
        super(lc);
        listData = FXCollections.observableArrayList();
        //PUT EVERYTHING IN THE COMPONENTS LIST INTO THIS LIST
        for(String s: lc.getTextList()){
            listData.add(s);
        }
        list.setItems(listData);
    }
    
    public void apply(){
        if(!isSameAsComponent()){ 
         ((ListComponent) component).setEdited(true);
        }
        ((ListComponent) component).setList(listData);
    }
    
    private boolean isSameAsComponent(){
        if(((ListComponent) component).getTextList().size() == listData.size()){
            for(int i = 0; i < listData.size(); i++){
                if(!listData.get(i).equals( ((ListComponent) component).getTextList().get(i) )){
                    return false;
                }
            }
            return true;
        }
        return false;
    }
    
    public void generateDialog(){
        Stage stage = new Stage();
        if(!editMode){
            stage.setTitle("Create list component");
        }else{
            stage.setTitle("Edit a list component");
        }
        stage.initModality(Modality.APPLICATION_MODAL);
        HBox root = new HBox();
        Scene scene = new Scene(root, 800, 300);
        
        root.setStyle("-fx-background-color: #9966ff;");
        
        VBox rightRegion = new VBox();
        rightRegion.setSpacing(5);
        rightRegion.setPadding(new Insets(10, 10, 10, 10));
        
        Button ok = new Button("OK");
        ok.setMaxWidth(Double.MAX_VALUE);
        
        ok.setOnAction((ActionEvent e) -> {
            if(!(listData.isEmpty())){
                apply();
                stage.close();
            }
        });
        
        Button addText = new Button("Add to List");
        Button removeText = new Button("Remove from List");
        
        addText.setOnAction((ActionEvent e) -> {
            if(!inputArea.getText().equals("")){
                listData.add(inputArea.getText());
                inputArea.clear();
            }
        });
        
        removeText.setOnAction((ActionEvent e) -> {
            listData.remove(list.getSelectionModel().getSelectedItem());
        });
        
        addText.setMaxWidth(Double.MAX_VALUE);
        removeText.setMaxWidth(Double.MAX_VALUE);
        
        //ADD OBSERVABLE LIST LATER FOR LISTVIEW
        
        rightRegion.getChildren().addAll(inputArea, addText, removeText, ok);
        
        root.setPadding(new Insets(10, 10, 10, 10));
        root.getChildren().addAll(list, rightRegion);
        
        stage.setScene(scene);
        stage.showAndWait();
    }
    
    public void activateEditMode(){
        editMode = true;
    }
}
