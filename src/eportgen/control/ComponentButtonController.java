/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportgen.control;

import Dialogs.AlertDialog;
import static eportgen.StartupConstants.UI_CSS;
import eportgen.model.Component;
import eportgen.model.HeaderComponent;
import eportgen.model.ImgComponent;
import eportgen.model.ListComponent;
import eportgen.model.Page;
import eportgen.model.ParagraphComponent;
import eportgen.model.SlideshowComponent;
import eportgen.model.VideoComponent;
import eportgen.view.PageEditView;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *USED TO HANDLE THE EVENTS FOR ANY BUTTON THAT CAN ADD A COMPONENT TO THE PAGEEDITVIEW LISTVIEW
 * @author Manan Singh
 */
public class ComponentButtonController {
    
    private PageEditView page;
    
    public ComponentButtonController(PageEditView page){
        this.page = page;
    }
    
    //THIS WILL GENERATE THE DIALOG FOR A EDITING PARAGRAPH COMPONENT
    public void addParagraphComponent(){
        ParagraphComponent pc = new ParagraphComponent();
        ParagraphEditControl pec = new ParagraphEditControl(pc);
        pec.generateDialog();
        if(pc.getText() != ""){
            if(pc.wasEdited()){
                Page p = page.getPage();
                p.addComponent(pc);
            }
        }
        //HAVE TO HANDLE THE CASE WHERE YOU SIMPLY CANCEL THE DIALOG 
    }
    
    public void addHeaderComponent(){
        HeaderComponent he = new HeaderComponent();
        HeaderEditControl hec = new HeaderEditControl(he);
        hec.generateDialog();
        if(he.getText() != ""){
            Page p = page.getPage();
            p.addComponent(he);
        }
        //HAVE TO HANDLE THE CASE WHERE YOU SIMPLY CANCEL THE DIALOG 
    }
    
    public void addListComponent(){
        ListComponent lc = new ListComponent();
        ListEditControl lec = new ListEditControl(lc);
        lec.generateDialog();
        if(lc.wasEdited()){
            Page p = page.getPage();
            p.addComponent(lc);
        }
        //HAVE TO HANDLE THE CASE WHERE YOU SIMPLY CANCEL THE DIALOG 
    }
    
    public void addImgComponent(){
        ImgComponent ic = new ImgComponent();
        ImgEditControl iec = new ImgEditControl(ic);
        iec.generateDialog();
        if(ic.wasEdited()){
            Page p = page.getPage();
            p.addComponent(ic);
        }
        //HAVE TO HANDLE THE CASE WHERE YOU SIMPLY CANCEL THE DIALOG 
    }
    
    public void addVideoComponent(){
        VideoComponent vc = new VideoComponent();
        VideoEditControl vec = new VideoEditControl(vc);
        vec.generateDialog();
        if(vc.wasEdited()){
            Page p = page.getPage();
            p.addComponent(vc);
        }
        //HAVE TO HANDLE THE CASE WHERE YOU SIMPLY CANCEL THE DIALOG 
    }
    
    public void addSlideshowComponent(){
        SlideshowComponent sc = new SlideshowComponent();
        SlideshowEditControl sec = new SlideshowEditControl(sc);
        sec.generateDialog();
        if(sc.wasEdited()){
            Page p = page.getPage();
            p.addComponent(sc);
        }
        //HAVE TO HANDLE THE CASE WHERE YOU SIMPLY CANCEL THE DIALOG 
    }
    
    public void removeComponent(){
        if(page.getPage().getComponents().size() > 0){
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            VBox root = new VBox();
            root.setSpacing(10);
            root.setPadding(new Insets(10, 10, 10, 10));
            Scene scene = new Scene(root, 500, 500);
        
            Text prompt = new Text("Pick an element from the list below to remove:");
            ListView view = new ListView();
            view.setItems(page.getPage().getComponents());
            
            
            Button ok = new Button("OK");
            ok.setOnAction((ActionEvent e) -> {
                if(!view.getSelectionModel().isEmpty()){
                    page.getPage().getComponents().remove((Component)view.getSelectionModel().getSelectedItem());
                    stage.close();
                }else{
                    AlertDialog.alert("Pick a component to remove by clicking on the list");
                }
            });
            
            root.getChildren().addAll(prompt, view, ok);
            
            scene.getStylesheets().add(UI_CSS);
            root.getStyleClass().add("componentEditView-style");
        
            stage.setScene(scene);
            stage.showAndWait();
        }else{
            AlertDialog.alert("There are no components to remove!");
        }
    }
    
}
