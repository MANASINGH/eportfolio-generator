/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportgen.file;

import Dialogs.AlertDialog;
import SlideshowMaker.Slide;
import static eportgen.StartupConstants.SAVED_PORTFOLIOS;
import eportgen.control.PagesControl;
import eportgen.model.Component;
import eportgen.model.EPortModel;
import eportgen.model.HeaderComponent;
import eportgen.model.ImgComponent;
import eportgen.model.ListComponent;
import eportgen.model.Page;
import eportgen.model.ParagraphComponent;
import eportgen.model.SlideshowComponent;
import eportgen.model.VideoComponent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonWriter;


/**
 *
 * @author Manan Singh
 */
public class FileOperator {
    
    private EPortModel model;
    
    
    
    public FileOperator(EPortModel model){
        this.model = model;
    }
    
    //SAVES THE EPORTFOLIO
    public void save() throws IOException{
        initSaveDirectory();
        
        //CREATES THE FOLDER WHERE THE JSON FILES FOR THIS PORTFOLIO WILL GO
        File portfolioFolder = new File(SAVED_PORTFOLIOS + model.getTitle());
        if(!portfolioFolder.exists()){
            portfolioFolder.mkdir();
        }
        
        //FIRST DELETE EVERYTHING IN THIS DIRECTORY SO THERE ARE NOW LEFTOVER JSON FILES
        File[] jsonFilesInDir = portfolioFolder.listFiles();
        for(File f: jsonFilesInDir){
            Files.delete(f.toPath());
        }
        
        //CREATE A JSON ARRAY THAT WILL CONTAIN THE NAMES OF ALL THE PAGES IN THE SITE
        JsonArrayBuilder pageTitles = Json.createArrayBuilder();
        for(Page p: model.getPages()){
            pageTitles.add(p.getPageTitle());
        }
        JsonArray pageTitlesArray = pageTitles.build();
        
        //MAKE SURE YOU HANDLER THE CASE WHERE THERE ARE MULTIPLE PAGES WITH THE SAME NAME
        for(Page p: model.getPages()){
            String jsonFilePath = SAVED_PORTFOLIOS + model.getTitle() + "/" + p.getPageTitle() +   ".json";
            JsonArray webpageContent = jsonifyPage(p);
            
            JsonObject pageToWrite = Json.createObjectBuilder()
                                    .add("portfoliotitle", model.getTitle())
                                    .add("studentname", model.getStudentName())
                                    .add("pagetitles", pageTitlesArray)
                                    .add("pagetitle", p.getPageTitle())
                                    .add("layout", p.getLayoutName())
                                    .add("font", p.getFontName())
                                    .add("color", p.getColorName())
                                    .add("bannerimgpath", p.getBannerImgPath())
                                    .add("bannerimgname", p.getBannerImgName())
                                    .add("footer", p.getFooter())
                                    .add("webpageContent", webpageContent)
                    .build();
            
        //INIT THE WRITER
        OutputStream os = null;
            try {
                os = new FileOutputStream(jsonFilePath);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                //HANDLER ERROR?
            }
        JsonWriter jsonWriter = Json.createWriter(os); 
        
        jsonWriter.writeObject(pageToWrite);
            
            try {
                os.close();
            } catch (IOException e) {
                //HANDLER ERROR
                e.printStackTrace();
            }
        }
    }
    
    public void load(File directoryFilePath) throws IOException{
        initSaveDirectory();
        
        String studentName = "";
        
        File[] jsonFiles = directoryFilePath.listFiles();
        ArrayList<Page> pagesToAdd = new ArrayList<Page>();
        
        for(File f: jsonFiles){
            String jsonFilePath = f.getAbsolutePath();
            // LOAD THE JSON FILE WITH ALL THE DATA
            JsonObject json = loadJSONFile(jsonFilePath);
            
            Page p = new Page();
            p.setPageTitle(json.getString("pagetitle"));
            p.setLayoutName(json.getString("layout"));
            p.setFontName(json.getString("font"));
            p.setColorName(json.getString("color"));
            p.setBannerImgPath(json.getString("bannerimgpath"));
            p.setBannerImgName(json.getString("bannerimgname"));
            p.setFooter(json.getString("footer"));
            //IF THE BANNER IMAGE IS NOT FOUND, THIS METHOD WILL SET THE BANNER IMG PATH AND NAME TO A NULL STRING
            p.checkIfBannerExists();
            
            
            //CHANGE THIS THING LATER ON IF YOU HAVE TIME
            studentName = json.getString("studentname");
            
            JsonArray webpageContent = json.getJsonArray("webpageContent");
            
            for(int i = 0; i < webpageContent.size(); i++){
                JsonObject jobj = webpageContent.getJsonObject(i);
                switch (jobj.getString("type")){
                    case "h1":
                        p.addComponent(readHeader(jobj));
                        break;
                    case "p":
                        p.addComponent(readParagraph(jobj));
                        break;
                    case "ul":
                        p.addComponent(readList(jobj));
                        break;
                    case "img":
                        p.addComponent(readImg(jobj));
                        break;
                    case "video":
                        p.addComponent(readVideo(jobj));
                        break;
                    case "slideshow":
                        p.addComponent(readSlideshow(jobj));
                        break;
                    default:
                        continue;
                }
            }
            pagesToAdd.add(p);
        }
        
        model.reset();
        model.setTitle(directoryFilePath.getName());
        model.setStudentName(studentName);
        for(Page page: pagesToAdd){
            model.addPage(page);
        }
        
    }
    
    public void export(){
        Exporter exporter = new Exporter(model);
        try {
            exporter.export(exporter.initExportDir());
        } catch (IOException e) {
            AlertDialog.alert("There was an issue in trying to export the portfolio");
            e.printStackTrace();
        }
    }
    
    public void exportToView(){
        Exporter exporter = new Exporter(model);
        try {
            exporter.exportToView();
        } catch (IOException e) {
            AlertDialog.alert("There was an issue in trying to view the portfolio");
            PagesControl.signalChange();
            e.printStackTrace();
        }
    }
    
    private HeaderComponent readHeader(JsonObject jobj){
        HeaderComponent hc = new HeaderComponent();
        hc.setText(jobj.getString("data"));
        return hc;
    }
    
    private ParagraphComponent readParagraph(JsonObject jobj){
        ParagraphComponent pc = new ParagraphComponent();
        pc.setText(jobj.getString("data"));
        pc.setFont(jobj.getString("dataFont"));
        return pc;
    }
    
    private ListComponent readList(JsonObject jobj){
        ListComponent lc = new ListComponent();
        JsonArray textArray = jobj.getJsonArray("data");
        for(int i = 0; i < textArray.size(); i++){
            lc.addText(textArray.getJsonObject(i).getString("bullet"));
        }
        return lc;
    }
    
    private ImgComponent readImg(JsonObject jsonObj){
        JsonObject jobj = jsonObj.getJsonObject("data");
        ImgComponent ic = null;
        String absolutePath = jobj.getString("image path") + jobj.getString("src");
        int width = jobj.getInt("width");
        int length = jobj.getInt("height");
        String caption = jobj.getString("caption");
        String floatingString = jobj.getString("floating");
        int floating = -1;
        if(floatingString.equals("left")){
            floating = 1;
        }else if(floatingString.equalsIgnoreCase("")){
            floating = 2;
        }else if(floatingString.equals("right")){
            floating = 3;
        }else{
            floating = 4;
        }
        ic = new ImgComponent(absolutePath, width, length, caption, floating);
        return ic;
    }
    
    private SlideshowComponent readSlideshow(JsonObject jsonObj){
        SlideshowComponent sc = new SlideshowComponent();
        JsonArray jsonSlidesArray = jsonObj.getJsonArray("data");
        for (int i = 0; i < jsonSlidesArray.size(); i++) {
	    JsonObject slideJso = jsonSlidesArray.getJsonObject(i);
            
            Slide slide = new Slide();
            slide.setImageFileName(slideJso.getString("image_file_name")); 
            slide.setImagePath(slideJso.getString("image_path")); 
            slide.setCaption(slideJso.getString("caption"));
	    
            sc.getSlides().add(slide);
	}
        return sc;
    }
    
    public VideoComponent readVideo(JsonObject jsonObj){
        JsonObject jobj = jsonObj.getJsonObject("data");
        VideoComponent vc = null;
        String absolutePath = jobj.getString("video path") + jobj.getString("src");
        int width = jobj.getInt("width");
        int length = jobj.getInt("height");
        String caption = jobj.getString("caption");
        vc = new VideoComponent(absolutePath, width, length, caption);
        return vc;
    }
    
    //IF THE DIRECTORY WHERE FILES ARE SUPPOSED TO BE SAVED TO DOES NOT EXIST, THEN CREATE IT YOURSELF
    private void initSaveDirectory(){
        File saveDirectory = new File(SAVED_PORTFOLIOS);
        if(!saveDirectory.exists()){
            saveDirectory.mkdir();
        }
    }
    
    //CREATES A JSON OBJECT FOR THE PAGE
    public JsonArray jsonifyPage(Page p){
        JsonArrayBuilder webpageContent = Json.createArrayBuilder();
                
        
        JsonObject jo = null;
        for(Component c: p.getComponents()){
            switch(c.toString()){
                case "Header":
                    jo  = writeHeader((HeaderComponent) c);
                    break;
                case "Paragraph": 
                    jo = writeParagraph((ParagraphComponent) c);
                    break;
                case "List":
                    jo = writeList((ListComponent) c);
                    break;
                case "Image":
                    if( ( (ImgComponent) c ).getImageFile() != null )
                        jo = writeImage((ImgComponent) c);
                    break;
                case "Video":
                    if( ( (VideoComponent) c ).getVideoFile() != null )
                        jo = writeVideo((VideoComponent) c);
                    break;
                case "Slideshow":
                    jo = writeSlideshow( ((SlideshowComponent) c) );
                    //HANDLE SLIDESHOW CASE
                    break;
                default:
            }
            if(jo != null)
                webpageContent.add(jo);
            
        }
        
        JsonArray content = webpageContent.build();
        
        return content;
    }
    
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
        InputStream is = new FileInputStream(jsonFilePath);
        JsonReader jsonReader = Json.createReader(is);
        JsonObject json = jsonReader.readObject();
        jsonReader.close();
        is.close();
        return json;
    } 
    
    private JsonObject writeHeader(HeaderComponent hc){
        JsonObject header = Json.createObjectBuilder()
                            .add("type", "h1")
                            .add("data", hc.getText())
                .build();
        return header;
    }
    
    private JsonObject writeParagraph(ParagraphComponent pc){
        JsonObject paragraph = Json.createObjectBuilder()
                            .add("type", "p")
                            .add("data", pc.getText())
                            .add("dataFont", pc.getFont())
                .build();
        return paragraph;
    }
    
    private JsonObject writeList(ListComponent lc){
        JsonArrayBuilder dataBuilder = Json.createArrayBuilder();
        for(String s: lc.getTextList()){
            JsonObject bullet = Json.createObjectBuilder()
                            .add("bullet", s)
                    .build();
            dataBuilder.add(bullet);
        }
        
        JsonArray data = dataBuilder.build();
        
        JsonObject list = Json.createObjectBuilder()
                            .add("type", "ul")
                            .add("data", data)
                .build();
        return list;
    }
    
    private JsonObject writeImage(ImgComponent ic){
        JsonObject data = Json.createObjectBuilder()
                            .add("src", ic.getImageFile().getName())
                            .add("width", ic.getWidth())
                            .add("height", ic.getLength())
                            .add("caption", ic.getCaption())
                            .add("floating", ic.getFloatingSide())
                            .add("image path", ic.getImagePath())
                .build();
        
        JsonObject image = Json.createObjectBuilder()
                            .add("type", "img")
                            .add("data", data)
                .build();
        
        return image;
    }
    
    private JsonObject writeVideo(VideoComponent vc){
        JsonObject data = Json.createObjectBuilder()
                            .add("src", vc.getVideoFile().getName())
                            .add("width", vc.getWidth())
                            .add("height", vc.getLength())
                            .add("caption", vc.getCaption())
                            .add("video path", vc.getVideoPath())
                .build();
        
        JsonObject video = Json.createObjectBuilder()
                            .add("type", "video")
                            .add("data", data)
                .build();
        
        return video;
    }
    
    private JsonObject writeSlideshow(SlideshowComponent sc){
        // BUILD THE SLIDES ARRAY
        JsonArray slidesJsonArray = makeSlidesJsonArray(sc.getSlides());
        
        JsonObject data = Json.createObjectBuilder()
                            .add("type", "slideshow")
                            .add("data", slidesJsonArray)
                .build();
        
        return data;
    }
    
    private JsonArray makeSlidesJsonArray(List<Slide> slides) {
        int i = 0;
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for (Slide slide : slides) {
            //String caption = ((SlideEditView)captionData.get(i)).getCaptionText();
	    JsonObject jso = makeSlideJsonObject(slide);
	    jsb.add(jso);
            i++;
        }
        JsonArray jA = jsb.build();
        return jA;        
    }
    
    private JsonObject makeSlideJsonObject(Slide slide) {
        JsonObject jso = Json.createObjectBuilder()
		.add("image_file_name", slide.getImageFileName())
		.add("image_path", slide.getImagePath())
                .add("caption", slide.getCaption())
		.build();
	return jso;
    }
}
