/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportgen.file;

import SlideshowMaker.Slide;
import static eportgen.StartupConstants.BASE_INDEX_PAGE;
import static eportgen.StartupConstants.BASE_JS;
import static eportgen.StartupConstants.COLOR_1;
import static eportgen.StartupConstants.COLOR_2;
import static eportgen.StartupConstants.COLOR_3;
import static eportgen.StartupConstants.COLOR_4;
import static eportgen.StartupConstants.COLOR_5;
import static eportgen.StartupConstants.EXPORTED;
import static eportgen.StartupConstants.FONT_1;
import static eportgen.StartupConstants.FONT_2;
import static eportgen.StartupConstants.FONT_3;
import static eportgen.StartupConstants.FONT_4;
import static eportgen.StartupConstants.FONT_5;
import static eportgen.StartupConstants.LAYOUT_1;
import static eportgen.StartupConstants.LAYOUT_2;
import static eportgen.StartupConstants.LAYOUT_3;
import static eportgen.StartupConstants.LAYOUT_4;
import static eportgen.StartupConstants.LAYOUT_5;
import static eportgen.StartupConstants.SAVED_PORTFOLIOS;
import static eportgen.StartupConstants.VIEW_DIRECTORY;
import eportgen.model.EPortModel;
import eportgen.model.ImgComponent;
import eportgen.model.Page;
import eportgen.model.SlideshowComponent;
import eportgen.model.VideoComponent;
import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;

/**
 *ASSISTS IN EXPORTING TO DIRECTORIES
 * @author Manan Singh
 */
public class Exporter {
    
    private EPortModel model;
    
    private File thisPortfolio = null;
    
    public Exporter(EPortModel model){
        this.model = model;
    }
    
    public void export(File exportDirectory) throws IOException {
        //MAKE SURE THE EXPORT DIRECTORY EXISTS
        File exportDir = exportDirectory;
        
        //CHECK IF THE DIRECTORY FOR THIS PORTFOLIO EXISTS
        thisPortfolio = new File(exportDir, model.getTitle());
        if(!thisPortfolio.exists()){
            thisPortfolio.mkdir();
        }
        
        FileUtils.cleanDirectory(thisPortfolio);
        
        //NOW MAKE A DIRECTORY FOR EACH PAGE IN THE PORTFOLIO
        for(Page page: model.getPages()){
            
            //CHECK IF THE SPECIFIC DIRECTORY FOR THIS PAGE EXISTS
            File thisPageDir = new File(thisPortfolio, page.getPageTitle());
            if(!thisPageDir.exists()){
                thisPageDir.mkdir();
            }
            
            //JUST CLEAN THE OLD DIRECTORY (DELETE ALL FILES WITHIN IT)
            FileUtils.cleanDirectory(thisPageDir);
            
            //ADD THE BASE HTML FILE FOR THE PAGE
            File pageHTML = new File(BASE_INDEX_PAGE);
            File thisHTMLPage = new File(thisPageDir, "index.html");
            FileUtils.copyFile(pageHTML, thisHTMLPage);
            
            
            //THEN ADD THE JSON FILE FOR THIS PAGE
            File jsonRef = new File(SAVED_PORTFOLIOS + model.getTitle()+ "/" + page.getPageTitle() + ".json");
            File nameSavedAs = new File(thisPageDir, "PageData.json");
            FileUtils.copyFile(jsonRef, nameSavedAs);
            
            
            //CHECK THE CSS DIRECTORY 
            File pageCSS = new File(thisPageDir, "css");
            File cssLayout = new File(pageCSS, "layout");
            File cssColor = new File(pageCSS, "color");
            File cssFont = new File(pageCSS, "font");
            if(!pageCSS.exists()){
                pageCSS.mkdir();
                cssLayout.mkdir();
                cssFont.mkdir();
                cssColor.mkdir();
            }
            
            //ADD APPROPRIATE FILES TO THE CSS DIRECTORIES BY COPYING THEM THERE
            String layout = page.getLayoutName();
            String font = page.getFontName();
            String color = page.getColorName();
            
            //THIS IS USED TO SAVE WHATEVER FONT FILE GETS PUT IN THE CSS AS 'FONT.CSS'
            File sameFontName = new File(cssFont, "font.css");
            File sameLayoutName = new File(cssLayout, "layout.css");
            File sameColorName = new File(cssColor, "color.css");
            
            
            File layoutFile = getLayoutFile(layout);
            File fontFile = getFontFile(font);
            File colorFile = getColorFile(color);
            
            FileUtils.copyFile(layoutFile, sameLayoutName);
            FileUtils.copyFile(fontFile, sameFontName);
            FileUtils.copyFile(colorFile, sameColorName);
            
            
            //CHECK THE JAVASCRIPT DIRECTORY AND ADD BASE JS FILE
            File pageJSDir = new File(thisPageDir, "js");
            if(!pageJSDir.exists()){
                pageJSDir.mkdir();
            }
            File pageJS = new File(BASE_JS);
            FileUtils.copyFileToDirectory(pageJS, pageJSDir);
            
            //MAKE IMG AND VIDEO DIRECTORIES
            File imgDir = new File(thisPageDir, "imgs");
            File vidDir = new File(thisPageDir, "videos");
            
            for(ImgComponent ic: page.getAllImages()){
                File imgFile = ic.getImageFile();
                FileUtils.copyFileToDirectory(imgFile, imgDir);
            }
            
            //AND ADD ALL SLIDE SHOW IMAGES
            for(SlideshowComponent sc: page.getAllSlideshows()){
                for(Slide slide: sc.getSlides()){
                    if(!slide.getCaption().equals("") && !slide.getImagePath().equals("")){
                        File slideFileImg = new File(slide.getImagePath() + slide.getImageFileName());
                        FileUtils.copyFileToDirectory(slideFileImg, imgDir);
                    }
                }
            }
            
            //DONT FORGET THE BANNER IMAGE
            File bannerImg = new File(page.getBannerImgPath() + page.getBannerImgName());
            FileUtils.copyFileToDirectory(bannerImg, imgDir);
            
            for(VideoComponent vc: page.getAllVideos()){
                File vidFile = vc.getVideoFile();
                FileUtils.copyFileToDirectory(vidFile, vidDir); 
            }
        }
    }
    
    public void exportToView() throws IOException {
        //MAKE SURE THE EXPORT DIRECTORY EXISTS
        File exportDir = initViewDirectory();
        
        //CHECK IF THE DIRECTORY FOR THIS PORTFOLIO EXISTS
        thisPortfolio = new File(exportDir, model.getTitle());
        if(!thisPortfolio.exists()){
            thisPortfolio.mkdir();
        }
        
        //JUST CLEAN THE OLD DIRECTORY (DELETE ALL FILES WITHIN IT)
        FileUtils.cleanDirectory(exportDir);
        
        //NOW MAKE A DIRECTORY FOR EACH PAGE IN THE PORTFOLIO
        for(Page page: model.getPages()){
            
            //CHECK IF THE SPECIFIC DIRECTORY FOR THIS PAGE EXISTS
            File thisPageDir = new File(thisPortfolio, page.getPageTitle());
            if(!thisPageDir.exists()){
                thisPageDir.mkdir();
            }
            
            //ADD THE BASE HTML FILE FOR THE PAGE
            File pageHTML = new File(BASE_INDEX_PAGE);
            FileUtils.copyFileToDirectory(pageHTML, thisPageDir);
            
            
            //THEN ADD THE JSON FILE FOR THIS PAGE
            File jsonRef = new File(SAVED_PORTFOLIOS + model.getTitle()+ "/" + page.getPageTitle() + ".json");
            File nameSavedAs = new File(thisPageDir, "PageData.json");
            FileUtils.copyFile(jsonRef, nameSavedAs);
            
            
            //CHECK THE CSS DIRECTORY 
            File pageCSS = new File(thisPageDir, "css");
            File cssLayout = new File(pageCSS, "layout");
            File cssColor = new File(pageCSS, "color");
            File cssFont = new File(pageCSS, "font");
            if(!pageCSS.exists()){
                pageCSS.mkdir();
                cssLayout.mkdir();
                cssFont.mkdir();
                cssColor.mkdir();
            }
            
            //ADD APPROPRIATE FILES TO THE CSS DIRECTORIES BY COPYING THEM THERE
            String layout = page.getLayoutName();
            String font = page.getFontName();
            String color = page.getColorName();
            
            //THIS IS USED TO SAVE WHATEVER FONT FILE GETS PUT IN THE CSS AS 'FONT.CSS'
            File sameFontName = new File(cssFont, "font.css");
            File sameLayoutName = new File(cssLayout, "layout.css");
            File sameColorName = new File(cssColor, "color.css");
            
            
            File layoutFile = getLayoutFile(layout);
            File fontFile = getFontFile(font);
            File colorFile = getColorFile(color);
            
            FileUtils.copyFile(layoutFile, sameLayoutName);
            FileUtils.copyFile(fontFile, sameFontName);
            FileUtils.copyFile(colorFile, sameColorName);
            
            
            //CHECK THE JAVASCRIPT DIRECTORY AND ADD BASE JS FILE
            File pageJSDir = new File(thisPageDir, "js");
            if(!pageJSDir.exists()){
                pageJSDir.mkdir();
            }
            File pageJS = new File(BASE_JS);
            FileUtils.copyFileToDirectory(pageJS, pageJSDir);
            
            //MAKE IMG AND VIDEO DIRECTORIES
            File imgDir = new File(thisPageDir, "imgs");
            File vidDir = new File(thisPageDir, "videos");
            
            for(ImgComponent ic: page.getAllImages()){
                File imgFile = ic.getImageFile();
                FileUtils.copyFileToDirectory(imgFile, imgDir);
            }
            
            //DONT FORGET THE BANNER IMAGE
            File bannerImg = new File(page.getBannerImgPath() + page.getBannerImgName());
            FileUtils.copyFileToDirectory(bannerImg, imgDir);
            
            //AND ADD ALL SLIDE SHOW IMAGES
            for(SlideshowComponent sc: page.getAllSlideshows()){
                for(Slide slide: sc.getSlides()){
                    if(!slide.getCaption().equals("") && !slide.getImagePath().equals("")){
                        File slideFileImg = new File(slide.getImagePath() + slide.getImageFileName());
                        System.out.println(slide.getImagePath() + slide.getImageFileName());
                        FileUtils.copyFileToDirectory(slideFileImg, imgDir);
                    }
                }
            }
            
            for(VideoComponent vc: page.getAllVideos()){
                File vidFile = vc.getVideoFile();
                FileUtils.copyFileToDirectory(vidFile, vidDir); 
            }
        }
    }
    
    
    
    //INITIALIZES THE DIRECTORY THAT THE PROGRAM EXPORTS TO IF IT DOESN'T ALREADY EXIST
    public File initExportDir(){
        File exportDir = new File(EXPORTED);
        if(!exportDir.exists()){
            exportDir.mkdir();
        }
        return exportDir
        ;
    }
    
    //INITIALIZES THE DIRECTORY THAT THE PROGRAM WILL USE TO DISPLAY THE WEBPAGGE
    public File initViewDirectory(){
        File viewDir = new File(VIEW_DIRECTORY);
        if(!viewDir.exists()){
            viewDir.mkdir();
        }
        return viewDir
        ;
    }
    
    //ADDS IMAGES INTO AN EXPORT DIRECTORY
    private void addImgs(){
        
    }
    
    //ADDS VIDEOS INTO AN EXPORT DIRECTORY
    private void addVideos(){
    
    }
    
    //ADDS JSON FILES INTO AN EXPORT DIRECTORY
    private void addJSONFiles(){
        
    }
    
    //RETURNS THE CORRECT BASE LAYOUT FILE TO THE EXPORT DIRECTORY
    private File getLayoutFile(String layoutName){
        File l = null;
        switch (layoutName) {
            case "Layout 1":
                l = new File(LAYOUT_1);
                return l;
            case "Layout 2":
                l = new File(LAYOUT_2);
                return l;
            case "Layout 3":
                l = new File(LAYOUT_3);
                return l;
            case "Layout 4":
                l = new File(LAYOUT_4);
                return l;
            case "Layout 5":
                l = new File(LAYOUT_5);
                return l;
            default:
                //RETURN THE FIRST LAYOUT AS DEFAULT
                l = new File(LAYOUT_1);
        }
        return l;
    }
    
    //RETURNS THE CORRECT BASE FONT FILE TO THE EXPORT DIRECTORY
    private File getFontFile(String fontName){
        File l = null;
        switch (fontName) {
            case "Oswald":
                l = new File(FONT_1);
                return l;
            case "Ubuntu":
                l = new File(FONT_2);
                return l;
            case "Francois One":
                l = new File(FONT_3);
                return l;
            case "Montserrat":
                l = new File(FONT_4);
                return l;
            case "Roboto":
                l = new File(FONT_5);
                return l;
            default:
                //RETURN THE FIRST FONT AS DEFAULT
                l = new File(FONT_1);
        }
        return l;
    }
    
    //RETURNS THE CORRECT BASE COLOR FILE TO THE EXPORT DIRECTORY
    private File getColorFile(String colorName){
        File l = null;
        switch (colorName) {
            case "Color Scheme 1":
                l = new File(COLOR_1);
                return l;
            case "Color Scheme 2":
                l = new File(COLOR_2);
                return l;
            case "Color Scheme 3":
                l = new File(COLOR_3);
                return l;
            case "Color Scheme 4":
                l = new File(COLOR_4);
                return l;
            case "Color Scheme 5":
                l = new File(COLOR_5);
                return l;
            default:
                //RETURN THE FIRST COLOR AS DEFAULT
                l = new File(COLOR_1);
        }
        return l;
    }
}
