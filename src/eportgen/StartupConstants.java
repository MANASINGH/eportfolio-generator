/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportgen;

/**
 *
 * @author Manan Singh
 */
public class StartupConstants {
    
    //PATHS TO USEFULL FOLDERS
    public static String DATA_PATH = "./data/";
    public static String IMAGES_PATH = "./images/";
    public static String ICONS_PATH = IMAGES_PATH + "icons/";
    public static String CSS_PATH = "eportgen/style/";
    public static String SAVED_PORTFOLIOS =  DATA_PATH + "eportfolios/";
    public static String EXPORTED = DATA_PATH + "exported/";
    public static String VIEW_DIRECTORY = DATA_PATH + "view/";
    
    //PATHS TO IMPORTANT FILES
    public static String UI_CSS = CSS_PATH + "ui_style.css";
    
    //PATHS TO INDIVIDUAL ICONS
    public static String NEW_PORTFOLIO = ICONS_PATH + "new_portfolio.png";
    public static String SAVE_PORTFOLIO = ICONS_PATH + "save_portfolio.png";
    public static String SAVEAS_PORTFOLIO = ICONS_PATH + "saveAs_portfolio.png";
    public static String OPEN_PORTFOLIO = ICONS_PATH + "open_portfolio.png";
    public static String EXIT = ICONS_PATH + "exit.png";
    public static String EXPORT = ICONS_PATH + "export.png";
    public static String EDIT_PAGE = ICONS_PATH + "edit_page.png";
    public static String VIEW_SITE = ICONS_PATH + "view_site.png";
    public static String ADD_SITE_PAGE = ICONS_PATH + "add_site_page.png";
    public static String REMOVE_SITE_PAGE = ICONS_PATH + "remove_site_page.png";
    public static String EDIT_PAGE_TITLE = ICONS_PATH + "edit_page_title.png";
    public static String EDIT_STUDENT_NAME = ICONS_PATH + "edit_stdnt_name.png";
    public static String ADD_TEXT_CMPNT = ICONS_PATH + "add_text_cmpnt.png";
    public static String ADD_VIDEO_CMPNT = ICONS_PATH + "add_video_cmpnt.png";
    public static String ADD_SLIDESHOW_CMPNT = ICONS_PATH + "add_slideshow_cmpnt.png";
    public static String ADD_IMG_CMPNT = ICONS_PATH + "add_img_cmpnt.png";
    public static String REMOVE_CMPNT = ICONS_PATH + "remove_cmpnt.png";
    public static String ADD_PARAGRAPH = ICONS_PATH + "add_paragraph.png";
    public static String ADD_HEADER = ICONS_PATH + "add_header.png";
    public static String ADD_LIST = ICONS_PATH + "add_list.png";
    
    //PATHS TO INDIVIDUAL IMAGES
    public static String DEFAULT_PICTURE = IMAGES_PATH + "DefaultPicture.png";
    
    //PATHS TO THE BASE FILES
    public static String BASE_FILES = DATA_PATH + "BASE_FILES/";
    public static String BASE_INDEX_PAGE = BASE_FILES + "index.html";
    public static String BASE_JS = BASE_FILES + "page.js";
    
    public static String BASE_CSS = BASE_FILES + "BASE_CSS/";
    public static String BASE_LAYOUT = BASE_CSS + "layout/";
    public static String BASE_COLOR = BASE_CSS + "color/";
    public static String BASE_FONT = BASE_CSS + "font/";
    
    //BASE CSS FONTS
    public static String FONT_1 = BASE_FONT + "font1.css"; //Oswald
    public static String FONT_2 = BASE_FONT + "font2.css"; //Ubuntu
    public static String FONT_3 = BASE_FONT + "font3.css"; //Francois One
    public static String FONT_4 = BASE_FONT + "font4.css"; //Montserrat
    public static String FONT_5 = BASE_FONT + "font5.css"; //Roboto
    
    //BASE CSS LAYOUTS
    public static String LAYOUT_1 = BASE_LAYOUT + "layout1.css";
    public static String LAYOUT_2 = BASE_LAYOUT + "layout2.css";
    public static String LAYOUT_3 = BASE_LAYOUT + "layout3.css";
    public static String LAYOUT_4 = BASE_LAYOUT + "layout4.css";
    public static String LAYOUT_5 = BASE_LAYOUT + "layout5.css";
    
    //BASE CSS COLOR SCHEMES
    public static String COLOR_1 = BASE_COLOR + "color1.css";
    public static String COLOR_2 = BASE_COLOR + "color2.css";
    public static String COLOR_3 = BASE_COLOR + "color3.css";
    public static String COLOR_4 = BASE_COLOR + "color4.css";
    public static String COLOR_5 = BASE_COLOR + "color5.css";
    
    
}
