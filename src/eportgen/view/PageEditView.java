/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportgen.view;

import Dialogs.BannerImgChooser;
import Dialogs.ColorChooser;
import Dialogs.FontChooser;
import Dialogs.FooterDialog;
import Dialogs.LayoutChooser;
import static eportgen.StartupConstants.ADD_HEADER;
import static eportgen.StartupConstants.ADD_IMG_CMPNT;
import static eportgen.StartupConstants.ADD_LIST;
import static eportgen.StartupConstants.ADD_PARAGRAPH;
import static eportgen.StartupConstants.ADD_SLIDESHOW_CMPNT;
import static eportgen.StartupConstants.ADD_TEXT_CMPNT;
import static eportgen.StartupConstants.ADD_VIDEO_CMPNT;
import static eportgen.StartupConstants.REMOVE_CMPNT;
import static eportgen.StartupConstants.UI_CSS;
import eportgen.control.ComponentButtonController;
import eportgen.control.PagesControl;
import eportgen.model.Component;
import eportgen.model.Page;
import java.io.File;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *CLASS IS USED FOR EDITING A SPECIFIC PAGE ON THE PORTFOLIO. IT CONTAINS CONTROLS TO ADD COMPONENTS ONTO THE PAGE AS WELL AS 
 *EDIT THEM. 
 * @author Manan Singh
 */
public class PageEditView extends BorderPane {
    
    //THE INTERNAL MODEL FOR THIS PAGE EDIT VIEW
    Page page;
    //CONTAINS THE CONTROLS TO EDIT NAME, LAYOUT, BANNER IMG, AND COLOR SCHEME
    private VBox pageInfoArea;
    //A LIST VIEW OF THE COMPONENTS THAT HAVE BEEN ADDED, EACH NODE IN THE LIST VIEW WILL BE A CLICKABLE BUTTON THAT WILL
    //DISPLAY A DIALOG (COMPONENTEDITVIEW) FOR THAT COMPONENT IF CLICKED
    private ListView<Component> addedComponents;
    //IS THE THE ACTUAL DATA THAT THE LIST VIEW WILL SHOW
 //   private ObservableList<Component> addedComponentData = FXCollections.observableArrayList();
    //CONTAINS THE CONTROLS FOR ADDING ALL SORTS OF COMPONENTS
    private HBox componentAddingArea;
    //A CONTROLLER THAT HANDLES EVENTS FOR BUTTONS THAT WILL ADD CONTENT TO THE WEBPAGE (EX: ADDTEXTCMPNT)
    private ComponentButtonController contentHandler = new ComponentButtonController(this);
    
    //CONTROLS AND TEXT FOR FOR PAGEINFORAREA
    private Button changeName;
    private Button changeLayout;
    private Button changeBannerImg;
    private Button changeColor;
    private Button changeFont;
    private Button changeFooter;
    
    private Text layoutName = new Text("N/A");
    private Text bannerImgName = new Text("N/A");
    private Text colorName = new Text("N/A");
    private Text fontName = new Text("N/A");
    
    //BUTTONS FOR ADDING COMPONENTS
    private Button addTextCmpnt;
    private Button addImgCmpnt;
    private Button addVideoCmpnt;
    private Button addSlideshowCmpnt;
    private Button removeCmpnt;
    
    //THIS WILL ALLOW THE USER TO SELECT A FONT FOR THE PAGE
    private FontChooser fontSelector;
    //THIS WILL ALLOW THE USER TO SELECT A LAYOUT
    private LayoutChooser layoutSelector;
    //THIS WILL ALLOW THE USER TO SELECT A COLOR
    private ColorChooser colorSelector;
    //THIS WILL ALLOW THE USER TO SELECT THE BANNER IMAGE
    private BannerImgChooser bannerSelector;
    
    public PageEditView(Page p){
        page = p;
        
        initPageInfoArea();
        initAddedComponents();
        initComponentAddingArea();
        initTextModifiers();
        initEventHandlers();
    }
    
    //PUT ALL THE GUI OBJECTS INTO THE RIGHT PLACE ON THE SCREEN FOR THIS PANE
    private void initPageInfoArea(){
        pageInfoArea = new VBox();
        pageInfoArea.setAlignment(Pos.CENTER);
        pageInfoArea.setPadding(new Insets(10, 10, 10, 50));
        pageInfoArea.setSpacing(20);
        
        HBox row1 = new HBox();
        row1.setSpacing(10);
        HBox row2 = new HBox();
        row2.setSpacing(10);
        HBox row3 = new HBox();
        row3.setSpacing(10);
        HBox row4 = new HBox();
        row4.setSpacing(10);
        HBox row5 = new HBox();
        row5.setSpacing(10);
        
        changeName = new Button("Change Student Name");
        changeLayout = new Button("Change Layout");
        changeBannerImg = new Button("Change Banner Image");
        changeColor = new Button("Change Color");
        changeFont = new Button("Change Font");
        changeFooter = new Button("Change Footer");
        
        changeName.setPrefWidth(200);
        changeLayout.setPrefWidth(200);
        changeBannerImg.setPrefWidth(200);
        changeColor.setPrefWidth(200);
        changeFont.setPrefWidth(200);
        changeFooter.setPrefWidth(200);
        
        
        row1.getChildren().add(changeFooter);
        row2.getChildren().addAll(changeLayout, layoutName);
        row3.getChildren().addAll(changeBannerImg, bannerImgName);
        row4.getChildren().addAll(changeColor, colorName);
        row5.getChildren().addAll(changeFont, fontName);
        
        pageInfoArea.getChildren().addAll(row1, row2, row3, row4, row5);
        
        this.setLeft(pageInfoArea);
    }
    
    private void initAddedComponents(){
        VBox listArea = new VBox();
        listArea.setAlignment(Pos.CENTER);
        listArea.setPadding(new Insets(10, 100, 10, 10));
        
        Text content = new Text("Webpage Content: ");
        
        addedComponents = new ListView<Component>();
        
        
        listArea.getChildren().addAll(content, addedComponents);
        
        //Test
        //page.addComponent(new ParagraphComponent("This is a paragraph text component!"));
        //page.addComponent(new HeaderComponent("This is a header text component!"));
        
        //ListComponent lc = new ListComponent();
        //lc.addText("This");
        //lc.addText("is");
        //lc.addText("a");
        //lc.addText("list");
        //lc.addText("text");
        //lc.addText("component");
        //page.addComponent(lc); 
        
        //page.addComponent(new ImgComponent(500, 500, "I love this picture!", 1));
        //page.addComponent(new VideoComponent("C:\\Users\\Manan Singh\\Videos\\ChessGame.mp4", 500, 500, "That was a close game!"));
        //page.addComponent(new SlideshowComponent());
        
        addedComponents.setItems(page.getComponents());
        addedComponents.setOnMouseClicked(new EventHandler<MouseEvent>(){
            public void handle(MouseEvent e){
                if(addedComponents.getSelectionModel().getSelectedItem() != null){
                    addedComponents.getSelectionModel().getSelectedItem().edit();
                    
                    //IF A COMPONENT WAS EDITED, REMOVE IT AND ADD IT BACK TO THE LIST SO THE LIST CHANGE LISTENER ENABLES THE SAVE BUTTONS
                    if(addedComponents.getSelectionModel().getSelectedItem().wasEdited()){
                        int index = addedComponents.getSelectionModel().getSelectedIndex();
                        Component c = addedComponents.getSelectionModel().getSelectedItem();
                        getPage().getComponents().remove(index);
                        getPage().getComponents().add(index, c);
                    }
                    
                    addedComponents.getSelectionModel().clearSelection();
                }
            }
        });
        
        
        addedComponents.getStyleClass().add("added-component-list");
        
        this.setCenter(listArea);
    }
    
    private void initComponentAddingArea(){
        HBox row = new HBox();
        row.setSpacing(10);
        row.setPadding(new Insets(10, 10, 300, 10));
        row.setAlignment(Pos.CENTER);
        
        addTextCmpnt = initAndAddButton(row, ADD_TEXT_CMPNT, "Add a text component to your webpage");
        addImgCmpnt = initAndAddButton(row, ADD_IMG_CMPNT, "Add an image to your webpage");
        addVideoCmpnt = initAndAddButton(row, ADD_VIDEO_CMPNT, "Add a video to your webpage");
        addSlideshowCmpnt = initAndAddButton(row, ADD_SLIDESHOW_CMPNT, "Add a slideshow to your webpage");
        removeCmpnt = initAndAddButton(row, REMOVE_CMPNT, "Remove a component from your webpage");
        
        this.setBottom(row);
    }
    
    private void initEventHandlers(){
        //EVENTS FOR THE PAGE INFO AREA
        changeFont.setOnAction((ActionEvent e) -> {
            //ALLOW USER TO CHANGE FONT
            String prevFont = fontSelector.getCurrentFont();
            fontSelector.createDialog();
            updateFontName();
            if(!prevFont.equals(fontSelector.getCurrentFont())){
                PagesControl.signalChange();
            }
        });
        
        changeLayout.setOnAction((ActionEvent e) -> {
            //ALLOW USER TO CHANGE LAYOUT
            String prevLayout = layoutSelector.getCurrentLayout();
            layoutSelector.createDialog();
            updateLayoutName();
            if(!prevLayout.equals(layoutSelector.getCurrentLayout())){
                PagesControl.signalChange();
            }
        });
        
        changeColor.setOnAction((ActionEvent e) -> {
            //ALLOW USER TO CHANGE COLOR SCHEME
            String prevColor = colorSelector.getCurrentColor();
            colorSelector.createDialog();
            updateColorName();
            if(!prevColor.equals(colorSelector.getCurrentColor())){
                PagesControl.signalChange();
            }
        });
        
        changeBannerImg.setOnAction((ActionEvent e) -> {
            //ALLOW USER TO CHANGE BANNER IMAGE
            File f = bannerSelector.getImageFile();
            bannerSelector.createDialog();
            bannerSelector.updateImageFile();
            if(!f.equals(bannerSelector.getImageFile())){
                PagesControl.signalChange();
            }
        });
        
        changeFooter.setOnAction((ActionEvent e) -> {
            FooterDialog fd = new FooterDialog(page);
            fd.createDialog();
        });
        
        //EVENTS FOR THE CONTROLS THAT ADD COMPONENTS
        addTextCmpnt.setOnAction((ActionEvent e) -> {
            selectTextComponentControl();
        });
        
        addImgCmpnt.setOnAction((ActionEvent e) -> {
            contentHandler.addImgComponent();
        }); 
        
        addVideoCmpnt.setOnAction((ActionEvent e) -> {
            contentHandler.addVideoComponent();
        });
        
        addSlideshowCmpnt.setOnAction((ActionEvent e) -> {
            contentHandler.addSlideshowComponent();
        });
        
        removeCmpnt.setOnAction((ActionEvent e) -> {
            contentHandler.removeComponent();
        });
    }
    
    //INITIALIZES CONTROLS THAT WILL MODIFY THE TEXT 
    private void initTextModifiers(){
        //INIT THE SELECTORS FOR THESE BUTTONS
         fontSelector = new FontChooser(fontName);
         if(page.getFontName().equals("")){
             //CALLING THIS FUNCTION LIKE THIS WILL MAKE START THE DEFAULT FONT OUT AS OSWALD
             updateFontName();
         }else{
             //THIS MEANS THAT THE PAGE WE'RE DEALING WITH ALREADY HAS FONT DATA, SO WE USE THE FONT DATA STORED INSIDE THE PAGE
             fontSelector.selectFont(page.getFontName());
             updateFontName();
         }
         //WHAT ARE YOU DO WHEN PAGES WITH PRESELECTED FONTS GET LOADED?
         
         layoutSelector = new LayoutChooser(layoutName);
         if(page.getLayoutName().equals("")){
             updateLayoutName();
         }else{
             layoutSelector.selectFont(page.getLayoutName());
             updateLayoutName();
         }
         
         
         colorSelector = new ColorChooser(colorName);
         if(page.getColorName().equals("")){
             updateColorName();
         }else{
             colorSelector.selectColor(page.getColorName());
         }
         
         bannerSelector = new BannerImgChooser(page, bannerImgName);
         if(page.getBannerImgPath().equals("")){
             bannerSelector.updateImageFile();
         }else{
             //THIS MEANS THAT THE PAGE WE'RE DEALING WITH ALREADY HAS IMAGE DATA, SO WE USE THE FONT DATA STORED INSIDE THE PAGE
             File f = new File(page.getBannerImgPath() + page.getBannerImgName());
             //MAKE SURE THE IMAGE EXISTS
             if(f.exists()){
                 bannerSelector.setImageFile(f);
             }
             bannerSelector.updateImageFile();
         }
    }
    
    public Page getPage(){
        return page;
    }
    
    public ListView<Component> getAddedComponents(){
        return addedComponents;
    }
    
    private Button initAndAddButton(Pane p, 
            String imagePath, String toolTipText){
        Button b = new Button();
        Image i = new Image("file:" + imagePath);
        b.setGraphic(new ImageView(i));
        Tooltip tt = new Tooltip(toolTipText);
        b.setTooltip(tt);
        p.getChildren().add(b);
        b.setPrefHeight(75);
        b.setPrefWidth(75);
        return b;
    }
    
    private void updateFontName(){
        page.setFontName(fontSelector.getCurrentFont());
    }
    
    private void updateLayoutName(){
        page.setLayoutName(layoutSelector.getCurrentLayout());
    }
    
    private void updateColorName(){
        page.setColorName(colorSelector.getCurrentColor());
    }
    
    
    //USED WHEN THE USER SELECTS 'ADD A TEXT COMPONENT' BUTTON
    private void selectTextComponentControl(){
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setTitle("What would you like to do?");
        HBox root = new HBox();
        root.setPadding(new Insets(10, 10, 10, 10));
        root.setSpacing(10);
        root.setAlignment(Pos.CENTER);
        Scene scene = new Scene(root, 500, 100);
        scene.getStylesheets().add(UI_CSS);
        
        root.getStyleClass().add("default-style");
        
        Button addParagraph = null;
        Button addHeader = null;
        Button addList = null;
        
        addParagraph = initAndAddButton(root, ADD_PARAGRAPH, "Add a paragraph component to your website");
        addHeader = initAndAddButton(root, ADD_HEADER, "Add a header component to your website");
        addList = initAndAddButton(root, ADD_LIST, "Add a list component to your website");
        
        addParagraph.setOnAction((ActionEvent e) -> {
            stage.close();
            contentHandler.addParagraphComponent();
        });
        
        addHeader.setOnAction((ActionEvent e) -> {
            stage.close();
            contentHandler.addHeaderComponent();
        });
        
        addList.setOnAction((ActionEvent e) -> {
            stage.close();
            contentHandler.addListComponent();
        }); 
        
        stage.setScene(scene);
        stage.show();
    }
}
