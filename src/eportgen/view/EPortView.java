/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportgen.view;

import static eportgen.StartupConstants.ADD_SITE_PAGE;
import static eportgen.StartupConstants.NEW_PORTFOLIO;
import static eportgen.StartupConstants.SAVE_PORTFOLIO;
import static eportgen.StartupConstants.SAVEAS_PORTFOLIO;
import static eportgen.StartupConstants.OPEN_PORTFOLIO;
import static eportgen.StartupConstants.EXIT;
import static eportgen.StartupConstants.EXPORT;
import static eportgen.StartupConstants.VIEW_SITE;
import static eportgen.StartupConstants.EDIT_PAGE;
import static eportgen.StartupConstants.EDIT_PAGE_TITLE;
import static eportgen.StartupConstants.EDIT_STUDENT_NAME;
import static eportgen.StartupConstants.REMOVE_SITE_PAGE;
import static eportgen.StartupConstants.UI_CSS;
import static eportgen.StartupConstants.VIEW_DIRECTORY;
import eportgen.control.IOControl;
import eportgen.control.PagesControl;
import eportgen.model.Component;
import eportgen.model.EPortModel;
import eportgen.model.Page;
import java.io.File;
import java.net.MalformedURLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ListChangeListener;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.scene.web.WebView;
import javafx.stage.Screen;
import javafx.stage.Stage;



/**
 *
 * @author Manan Singh
 */
public class EPortView {
    
    //THE WINDOW FOR THIS APP
    Stage primaryStage;
    
    //THE MODEL THAT THE VIEW WILL BE BASED OFF OF
    private EPortModel model;
    
    //SPLITS THE ENTIRE APP UP INTO REGIONS SO THAT WORKSPACE, BUTTON, AND TOOLBARS CAN BE PLACED APPROPRIATLY 
    private BorderPane regions;
    
    //BASIC BUTTONS
    private Button newPortfolio;
    private Button openPortfolio;
    private Button savePortfolio;
    private Button saveAsPortfolio;
    private Button export;
    private Button exit;
    
    private Button editPage;
    private Button viewSite;
    
    private Button addSitePage;
    private Button removeSitePage;
    private Button editPageTitle;
    private Button editStudentName;
    
    //A LAYOUT PANE FOR THE TOP OF THE APP WHERE THE TOOLBAR AND TITLE WILL GO
    private VBox topSide;
    //THE PANE WHERE THE TITLE WILL BE STORED
    private VBox titleBoxArea;
    
    //THE TITLE OF THE EPORTFOLIO
    private Text title;
    //THE STUDENT NAME OF THE EPORTFOLIO
    private Text studentName;
    
    //THE DISPLAY PANE THAT WILL SHOW THE WORKSPACE
    private TabPane sitePages;
    
    //THE DISPLAY THAT WILL SHOW THE WEBSITE VIEW
    private WebView webview;
    
    //THE HANDLER FOR ALL THE FILE IO EVENTS AS WELL AS FOR SWITCHING BETWEEN WORKSPACES
    private IOControl ioController = new IOControl(this);
    
    private PagesControl pageController = new PagesControl(this);
    
    public EPortView(){
        //SET THE MODEL FOR THIS VIEW
        model = new EPortModel(this);
    }
    
    //CREATE THE WINDOW AND DISPLAY THE APPLICATION
    public void startUI(Stage primaryStage){
        this.primaryStage = primaryStage;
        primaryStage.setTitle("ePortfolio Generator");
        regions = new BorderPane();
        Scene scene = new Scene(regions);
        scene.getStylesheets().add(UI_CSS);
        
        initWindow(primaryStage);
        initToolbars();
        initWorkspace();
        initEventHandlers();
        
        //ENABLE/DISABLE APPROPRIATE BUTTONS
        disableSaveButton();
        disableExportButton();
        disableEditPageButton();
        disableViewSiteButton();
        disableAddPage();
        disableRemovePage();
        disableEditPageTitle();
        disableEditStudentName();
        disableSaveAsButton();
        
        regions.getStyleClass().add("default-style");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
    
    
    private void initWorkspace(){
        //FOR THE PAGE EDIT MODE
        sitePages = new TabPane();
        sitePages.setTabClosingPolicy(TabPane.TabClosingPolicy.UNAVAILABLE);
        sitePages.getStyleClass().add("floating"); //CSS STUFF
        regions.setCenter(sitePages);
        
        webview = new WebView();
    }
    
    private void initToolbars(){
        //AREA WILL HOLD TOOL BOX AREA AND THE TITLE AREA
        topSide = new VBox();
        //AREA WILL HOLD ALL OF THE TOOLBOXES THAT WILL CONTAIN BUTTONS
        HBox toolBoxArea = new HBox();
        //FOR BUTTONS: SAVE, SAVE AS, OPEN, EXIT, EXPORT AND NEW
        HBox ioButtonArea = new HBox();
        //FOR BUTTONS THAT WILL SWITCH THE WORKSPACE BETWEEN PAGE EDIT AND SITE VIEW MODE
        HBox workspaceModeArea = new HBox();
        //WHERE THE TITLE WILL GO WHEN A PROJECT IS STARTED/LOADED
        titleBoxArea = new VBox();
        //FOR BUTTONS THAT WILL EDIT WHAT PAGE ON THE WEBSITE YOU'RE ON
        VBox siteBarArea = new VBox();
        
        //ADD STYLE CLASS FOR GUI ELEMENTS
        toolBoxArea.getStyleClass().add("top-toolbar");
        siteBarArea.getStyleClass().add("paddedButton");
        titleBoxArea.setAlignment(Pos.CENTER);
        
        newPortfolio = initAndAddButton(ioButtonArea, NEW_PORTFOLIO, "Create a new portfolio");
        openPortfolio = initAndAddButton(ioButtonArea, OPEN_PORTFOLIO, "Open a portfolio");
        savePortfolio = initAndAddButton(ioButtonArea, SAVE_PORTFOLIO, "Save portfolio");
        saveAsPortfolio = initAndAddButton(ioButtonArea, SAVEAS_PORTFOLIO, "Save portfolio as");
        export = initAndAddButton(ioButtonArea, EXPORT, "Export portfolio");
        exit = initAndAddButton(ioButtonArea, EXIT, "Exit application");
        
        editPage = initAndAddButton(workspaceModeArea, EDIT_PAGE, "Edit your website");
        viewSite = initAndAddButton(workspaceModeArea, VIEW_SITE, "View your website");
        
        addSitePage = initAndAddButton(siteBarArea, ADD_SITE_PAGE, "Add a page to your website");
        removeSitePage = initAndAddButton(siteBarArea, REMOVE_SITE_PAGE, "Remove a page to your website");
        editPageTitle = initAndAddButton(siteBarArea, EDIT_PAGE_TITLE, "Edit the title of a specific page of your website");
        editStudentName = initAndAddButton(siteBarArea, EDIT_STUDENT_NAME, "Edit the student name of you website");
        
        toolBoxArea.getChildren().add(ioButtonArea);
        toolBoxArea.getChildren().add(workspaceModeArea);
        
        toolBoxArea.setSpacing(10);
        siteBarArea.setAlignment(Pos.CENTER);
        
        topSide.getChildren().addAll(toolBoxArea, titleBoxArea);
        
        regions.setTop(topSide);
        regions.setLeft(siteBarArea);
    }
    
    //CREATE THE WINDOW WITH THE CORRECT LENGTH AND WIDTH PROPERTIES
    private void initWindow(Stage primaryStage){
        // GET THE SIZE OF THE SCREEN
	Screen screen = Screen.getPrimary();
	Rectangle2D bounds = screen.getVisualBounds();

	// AND USE IT TO SIZE THE WINDOW
	primaryStage.setX(bounds.getMinX());
	primaryStage.setY(bounds.getMinY());
	primaryStage.setWidth(bounds.getWidth());
	primaryStage.setHeight(bounds.getHeight());
    }
    
    //CREATE THE EVENT HANDLERS THAT WILL GIVE THE VARIOUS TOOLBAR BUTTONS FUNCTIONALITY
    private void initEventHandlers(){
        //VIEW MODE TOOLBAR
        editPage.setOnAction((ActionEvent e) -> {
            switchToPageEdit();
            this.enableViewSiteButton();
            this.disableEditPageButton();
        });
        
        viewSite.setOnAction((ActionEvent e) -> {
            ioController.processViewSiteRequest();
            //switchToSiteView();
            //this.enableEditPageButton();
            //this.disableViewSiteButton();
        });
        
        //FILE IO TOOLBAR
        newPortfolio.setOnAction((ActionEvent e) -> {
            ioController.processNewPortfolioRequest();
        });
        
        savePortfolio.setOnAction((ActionEvent e) -> {
            ioController.processSaveRequest();
        });
        
        saveAsPortfolio.setOnAction((ActionEvent e) -> {
            ioController.promptToSaveAs();
        });
        
        openPortfolio.setOnAction((ActionEvent e) -> {
            ioController.processLoadRequest();
        });
        
        export.setOnAction((ActionEvent e) -> {
            ioController.processExportRequest();
        });
        
        exit.setOnAction((ActionEvent e) -> {
            ioController.proecessExitRequest();
        });
        
        //PAGE TOOLBAR
        addSitePage.setOnAction((ActionEvent e) -> {
            pageController.processAddPageRequest();
        });
        
        removeSitePage.setOnAction((ActionEvent e) -> {
            pageController.processRemovePageRequest();
        });
        
        editPageTitle.setOnAction((ActionEvent e) -> {
            pageController.processSetPageTitleRequest();
        });
        
        editStudentName.setOnAction((ActionEvent e) -> {
            pageController.processNewStudentNameRequest();
        });
    }
    
    //ADDS A NEW TAB TO THE VIEW WHICH WILL CONTAIN A NEW PAGE AND PAGE EDIT VIEW
    public void addNewTab(Page p){
        PageEditView pageEditor = new PageEditView(p);
        pageEditor.getStyleClass().add("pageEditor");
        //ADD A LISTENER THAT WILL ENABLE THE SAVE BUTTONS ANYTIME THE WEBPAGE CONTENT LIST FOR THE PAGE CHANGES
        pageEditor.getAddedComponents().getItems().addListener(new ListChangeListener<Component>(){

            @Override
            public void onChanged(ListChangeListener.Change<? extends Component> c) {
                enableSaveButton();
            }
            
        });
        Tab tab = new Tab(pageEditor.getPage().getPageTitle());
        tab.setContent(pageEditor);
        sitePages.getTabs().add(tab);
    }
    
    //REMOVES A TAB FROM THE VIEW BASED ON A PAGE
    public void removeTab(int index){
        sitePages.getTabs().remove(index);
    }
    
    //VISUALLY REMOVES ALL THE TABS AND BY EXTENSION THE PAGE EDIT VIEWS, THE MODEL WILL STILL HAVE THE PAGES THOUGH
    public void clearAllTabs(){
        sitePages.getTabs().clear();
    }
    
    private Button initAndAddButton(Pane p,  
            String imagePath, String toolTipText){
        Button b = new Button();
        Image i = new Image("file:" + imagePath);
        b.setGraphic(new ImageView(i));
        Tooltip tt = new Tooltip(toolTipText);
        b.setTooltip(tt);
        p.getChildren().add(b);
        b.setPrefHeight(50);
        b.setPrefWidth(50);
        return b;
    }
    
    //ADDS THE TITLE TO THE PROJECT AS A VISUAL ELEMENT
    public void addTitle(){
        updateTitle();
        updateStudentName();
        titleBoxArea.getChildren().add(title);
        title.getStyleClass().add("title");
        titleBoxArea.getChildren().add(studentName);
        studentName.getStyleClass().add("title");
    }
    
    //REMOVES THE TITLE FROM VIEW
    public void removeTitle(){
        titleBoxArea.getChildren().clear();
    }
    
    public Page getSelectedPage(){
        Tab t = sitePages.getSelectionModel().getSelectedItem();
        PageEditView pev = (PageEditView)t.getContent();
        Page p = pev.getPage();
        return p;
    }
    
    public Tab getSelctedTab(){
        Tab t = sitePages.getSelectionModel().getSelectedItem();
        return t;
    }
    
    //PROVIDES A WAY TO UPDATE THE TITLE VIEW
    public void updateTitle(){
        title = new Text("EPortfolio Title: " + model.getTitle());
    }
    
    public void updateStudentName(){
        studentName = new Text("Student Name: " + model.getStudentName());
    }
    
    public EPortModel getModel(){
        return model;
    }
    
    public void enableSaveButton(){
        savePortfolio.setDisable(false);
        ioController.setSaved(false);
    }
    
    public void disableSaveButton(){
        savePortfolio.setDisable(true);
        ioController.setSaved(true);
    }
    
    public void enableSaveAsButton(){
        saveAsPortfolio.setDisable(false);
    }
    
    public void disableSaveAsButton(){
        saveAsPortfolio.setDisable(true);
    }
    
    public void enableExportButton(){
        export.setDisable(false);
    }
    
    public void enableEditPageButton(){
        editPage.setDisable(false);
    }
    
    public void disableEditPageButton(){
        editPage.setDisable(true);
    }
    
    public void disableExportButton(){
        export.setDisable(true);
    }
    
    public void enableViewSiteButton(){
        viewSite.setDisable(false);
    }
    
    public void disableViewSiteButton(){
        viewSite.setDisable(true);
    }
    
    public void enableAddPage(){
        addSitePage.setDisable(false);
    }
    
    public void disableAddPage(){
        addSitePage.setDisable(true);
    }
    
    public void enableRemovePage(){
        removeSitePage.setDisable(false);
    }
    
    public void disableRemovePage(){
        removeSitePage.setDisable(true);
    }
    
    public void enableEditPageTitle(){
        editPageTitle.setDisable(false);
    }
    
    public void disableEditPageTitle(){
        editPageTitle.setDisable(true);
    }
    
    public void enableEditStudentName(){
        editStudentName.setDisable(false);
    }
    
    public void disableEditStudentName(){
        editStudentName.setDisable(true);
    }
    
    public void disableNewPortfolio(){
        newPortfolio.setDisable(true);
    }
    
    public void enableNewPortfolio(){
        newPortfolio.setDisable(false);
    }
    
    public void enableOpenPortfolio(){
        openPortfolio.setDisable(false);
    }
    
    public void disableOpenPortfolio(){
        openPortfolio.setDisable(true);
    }
    
    
    
    public Stage getWindow(){
        return primaryStage;
    }
    
    
    //SWITCH THE VIEW OF THE EPORTVIEW TO THE PAGE EDITOR
    public void switchToPageEdit(){
        webview.getEngine().load(null);
        regions.setCenter(this.sitePages);
        
        this.disableEditPageButton();
        this.enableViewSiteButton();
        this.enableViewSiteButton();
        this.enableSaveAsButton();
        this.enableAddPage();
        this.enableRemovePage();
        this.enableExportButton();
        this.enableSaveButton();
        this.enableEditPageTitle();
        this.enableEditStudentName();
        this.enableNewPortfolio();
        this.enableOpenPortfolio();
    }
    
    //SWITCH THE VIEW OF THE EPORTVIEW TO THE SITE VIEW
    public void switchToSiteView(){
        regions.setCenter(webview);
        
        //TEMPORARY URL FOR NOW 
        File webpage = new File(VIEW_DIRECTORY + model.getTitle() + "/" + getSelectedPage().getPageTitle() + "/index.html");
        try {
            webview.getEngine().load(webpage.toURI().toURL().toString());
        } catch (MalformedURLException ex) {
            Logger.getLogger(EPortView.class.getName()).log(Level.SEVERE, null, ex);
        }
	TextField addressBar = new TextField(webview.getEngine().getLocation());
	addressBar.setStyle("-fx-background-color: lightblue");
	addressBar.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>(){
		public void handle(KeyEvent e) {
			if(e.getCode() == KeyCode.ENTER){
				webview.getEngine().load(addressBar.getText());
				addressBar.setText(webview.getEngine().getLocation());
			}
		}
			
	});
    }
}
