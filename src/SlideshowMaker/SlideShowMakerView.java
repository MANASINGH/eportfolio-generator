/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SlideshowMaker;

import java.util.ArrayList;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author Manan Singh
 */
public class SlideShowMakerView extends Stage {
        
        private VBox slideArea = new VBox();
        private ArrayList images = new ArrayList();
        private ArrayList captions = new ArrayList();
        private BorderPane root = new BorderPane();
        private HBox menuBar = new HBox();
        
        Button addSlide = new Button("Add Slide");
        Button removeSlide = new Button("Remove Slide");
        Button slideUp = new Button("Move Slide Up");
        Button slideDown = new Button("Move Slide Down");
        
        public SlideShowMakerView(){
            this.setTitle("Make a slideshow");
            Scene scene = new Scene(root, 500, 700);
            
            menuBar.setSpacing(10);
            menuBar.setPadding(new Insets(10, 10, 10, 10));
            menuBar.getChildren().addAll(addSlide, removeSlide, slideUp, slideDown);
            root.setTop(menuBar);
            
            root.setCenter(slideArea);
            
            
            this.setScene(scene);
            this.showAndWait();
        }
}
