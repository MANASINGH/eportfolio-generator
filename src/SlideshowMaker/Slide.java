package SlideshowMaker;

import static eportgen.StartupConstants.IMAGES_PATH;

/**
 * This class represents a single slide in a slide show.
 * 
 * @author McKilla Gorilla & Manan Singh
 */
public class Slide {
    String imageFileName;
    String imagePath;
    String caption;
    private boolean edited = false;
     
    /**
     * Constructor, it initializes all slide data.
     * @param initImageFileName File name of the image.
     * 
     * @param initImagePath File path for the image.
     * 
     */
    public Slide(String initImageFileName, String initImagePath) {
	imageFileName = initImageFileName;
	imagePath = initImagePath;
        caption = new String();
    }
    
    public Slide(String initImageFileName, String initImagePath, String caption) {
	this(initImageFileName, initImagePath);
        this.caption = caption;
    }
    
    //FOR TESTING PURPOSES ONLY
    public Slide(){
        this("DefaultPicture.png", IMAGES_PATH);
    }
    
    // ACCESSOR METHODS
    public String getImageFileName() { return imageFileName; }
    public String getImagePath() { return imagePath; }
    
    // MUTATOR METHODS
    public void setImageFileName(String initImageFileName) {
	imageFileName = initImageFileName;
    }
    
    public void setImagePath(String initImagePath) {
	imagePath = initImagePath;
    }
    
    public void setImage(String initPath, String initFileName) {
	imagePath = initPath;
	imageFileName = initFileName;
    }
    
    public String getCaption(){
        return caption;
    }
    
    public void setCaption(String s){
        caption = s;
    }
    
    public Slide getCopy(){
        Slide slide = new Slide(this.imageFileName, this.imagePath, this.caption);
        return slide;
    }

    /**
     * @return the edited
     */
    public boolean isEdited() {
        return edited;
    }

    /**
     * @param edited the edited to set
     */
    public void setEdited(boolean edited) {
        this.edited = edited;
    }
}
