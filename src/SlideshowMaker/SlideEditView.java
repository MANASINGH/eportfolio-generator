package SlideshowMaker;

import static eportgen.StartupConstants.DEFAULT_PICTURE;
import java.io.File;
import java.net.URL;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * This UI component has the controls for editing a single slide
 * in a slide show, including controls for selected the slide image
 * and changing its caption.
 * 
 * @author McKilla Gorilla & Manan Singh
 */
public class SlideEditView extends HBox {
    // SLIDE THIS COMPONENT EDITS
    Slide slide;
    
    // DISPLAYS THE IMAGE FOR THIS SLIDE
    ImageView imageSelectionView;
    
    // CONTROLS FOR EDITING THE CAPTION
    VBox captionVBox;
    Label captionLabel = new Label("Caption");
    TextField captionTextField;
    
    // PROVIDES RESPONSES FOR IMAGE SELECTION
    ImageSelectionController imageController;
    
    
    
    // THE VARIABLE THAT WILL TRACK WHICH SLIDE IS HIGHLIGHTED
    public static SlideEditView selected;

    /**
     * THis constructor initializes the full UI for this component, using
     * the initSlide data for initializing values./
     * 
     * @param initSlide The slide to be edited by this component.
     */
    public SlideEditView(Slide initSlide) {
	
	
	// KEEP THE SLIDE FOR LATER
	slide = initSlide;
	
	// MAKE SURE WE ARE DISPLAYING THE PROPER IMAGE
	imageSelectionView = new ImageView(new Image("file:" + DEFAULT_PICTURE));
	updateSlideImage();

	// SETUP THE CAPTION CONTROLS
	captionVBox = new VBox();
	captionTextField = new TextField();
	captionVBox.getChildren().add(captionLabel);
	captionVBox.getChildren().add(captionTextField);

	// LAY EVERYTHING OUT INSIDE THIS COMPONENT
	getChildren().add(imageSelectionView);
	getChildren().add(captionVBox);

	// SETUP THE EVENT HANDLERS
	imageController = new ImageSelectionController();
	imageSelectionView.setOnMousePressed(e -> {
	    imageController.processSelectImage(slide, this);
	});
        
        // ADD AN EVENT HANDLER WHICH HIGHLIGHTS SELECTED SLIDE
        this.setOnMouseClicked(new EventHandler<MouseEvent>(){
            public void handle(MouseEvent e){
                becomeSelected();
            }
        });
        
        captionTextField.setOnKeyReleased(new EventHandler<KeyEvent>(){
            public void handle(KeyEvent e){
                String oldText = slide.getCaption();
                slide.setCaption(captionTextField.getText()); 
                slide.setEdited(true);
            }
        });
    }
    
    public void becomeSelected(){
        if(selected != null){
           selected.setStyle("-fx-border-style:");
        }
        selected = this;
        selected.setStyle("-fx-border-style: solid; -fx-border-width: 5px;");
        
    }
    
    /**
     * This function gets the image for the slide and uses it to
     * update the image displayed.
     */
    public void updateSlideImage() {
	String imagePath = slide.getImagePath() + "/" + slide.getImageFileName();
	File file = new File(imagePath);
	try {
	    // GET AND SET THE IMAGE
	    URL fileURL = file.toURI().toURL();
	    Image slideImage = new Image(fileURL.toExternalForm());
	    imageSelectionView.setImage(slideImage);
	    
	    // AND RESIZE IT
	    double scaledWidth = 200;
	    double perc = scaledWidth / slideImage.getWidth();
	    double scaledHeight = slideImage.getHeight() * perc;
	    imageSelectionView.setFitWidth(scaledWidth);
	    imageSelectionView.setFitHeight(scaledHeight);
	} catch (Exception e) {
	    // @todo - use Error handler to respond to missing image
            e.printStackTrace();
	}
    }
    
    public String getCaptionText(){
        return captionTextField.getText();
    }
    
    public void setCaptionText(String s){
        captionTextField.setText(s);
    }
    
    public static SlideEditView getSelected(){
        return selected;
    }
    
    public static void selectSlide(SlideEditView sev){
        if(selected != null){
            selected.setStyle("-fx-border-style:");
        }
        selected = sev;
        selected.setStyle("-fx-border-style: solid; -fx-border-width: 5px;");
        
    }
    
    public static void nullify(){
        selected = null;
        
    }
    
    public Slide getSlide(){
        return slide;
    }
    
}